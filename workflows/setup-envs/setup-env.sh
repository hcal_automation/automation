#!/usr/bin/env bash

if test "$BASH" = "" || "$BASH" -uc "a=();true \"\${a[@]}\"" 2>/dev/null; then
    set -euo pipefail
else
    set -eo pipefail
fi

echo "Starting creation of environment areas"
shopt -s expand_aliases
echo "Sourcing CMSSW Defaults"
source /cvmfs/cms.cern.ch/cmsset_default.sh

export HCAL_WORKING_AREA=$_CONDOR_SCRATCH_DIR

function setupPythonEnvironment(){
    mkdir -p $CMSSW_BASE/env_setup
    mkdir -p $CMSSW_BASE/bin/$SCRAM_ARCH

    ln -s $(which python3) $CMSSW_BASE/bin/$SCRAM_ARCH/

    cd $CMSSW_BASE/env_setup

    git clone https://${SERVICE_USERNAME}:${SERVICE_PASSWORD}@gitlab.cern.ch/cmsoms/oms-api-client.git
    cd oms-api-client
    python3 -m pip install --prefix $CMSSW_BASE/env_setup -I .

    cd $CMSSW_BASE/env_setup

    python3 -m pip install --prefix $CMSSW_BASE/env_setup -I influxdb
    python3 -m pip install --prefix $CMSSW_BASE/env_setup -I dbs3-client==4.0.19
    python3 -m pip install --prefix $CMSSW_BASE/env_setup -I urllib3==1.26.6
    python3 -m pip install --prefix $CMSSW_BASE/env_setup git+https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control/ --no-deps 
    python3 -m pip install --prefix $CMSSW_BASE/env_setup -I htcondor

    printf "Transfering binary and library files to area\n"
    cp -r $CMSSW_BASE/env_setup/bin/* $CMSSW_BASE/bin/$SCRAM_ARCH/
    cp -r $CMSSW_BASE/env_setup/lib/python*/site-packages/* $CMSSW_BASE/python/
    sed -i '1 s;#!.*python.*;#!/usr/bin/env python3;' $CMSSW_BASE/bin/$SCRAM_ARCH/*.py
    for f in $CMSSW_BASE/bin/$SCRAM_ARCH/*.py; do
        head $f
    done

    # And then cleanup env_setup area
    rm -rf $CMSSW_BASE/env_setup
}

function pedestals_production(){
    echo "Setting up pedestals production area"

    # CMSSW package for HcalNanoAOD production
    git clone https://${SERVICE_USERNAME}:${SERVICE_PASSWORD}@gitlab.cern.ch/cmshcal/hcalpfg/hcalnanoprod.git DPGAnalysis/HcalNanoProd

    scram b clean
    scram b -j8

    # Get private repo for processing HcalNano and producing pedestals txt
    git clone https://gitlab.cern.ch/hcal_automation/pedestals.git
    cd pedestals
    git checkout dev
    make -j8

    cd ${CMSSW_BASE}/src

    # Get calibrations repo with plotting tools for validating pedestals
    git clone https://${SERVICE_USERNAME}:${SERVICE_PASSWORD}@gitlab.cern.ch/cmshcal/hcalcalib.git
    cd  hcalcalib/AutomationScripts
    make -j8
}

function gains_production(){
    echo "Setting up gains production area"

    scram b clean
    scram b -j8

    # Get private repo for evaluating radiation damage
    git clone https://gitlab.cern.ch/rtaus/radiation-damage-reader.git

    # Get calibrations repo with plotting tools for validating pedestals
    git clone https://${SERVICE_USERNAME}:${SERVICE_PASSWORD}@gitlab.cern.ch/cmshcal/hcalcalib.git
    cd  hcalcalib/AutomationScripts
    make -j8

    cd ../..

    git clone https://gitlab.cern.ch/hcal_automation/gains.git
    cd gains
    make -j8
}

function automation_base(){
    echo "basic_area nothing to do"
}

function lut_production(){
    echo "Setting up LUT production area"

    git cms-addpkg CaloOnlineTools/HcalOnlineDb 

    git clone https://github.com/HcalConditionsAutomatization/ConditionsValidation ConditionsValidation
    cd CaloOnlineTools/HcalOnlineDb/test/
    cp -f $CMSSW_BASE/src/ConditionsValidation/LUTFigureParameters/PlotLUT.py PlotLUT.py

    cd $CMSSW_BASE/src

    scram b clean
    scram b -j8
}

function l1t_validation(){
    echo "Setting up l1t_validation"

    git cms-addpkg L1Trigger/L1TNtuples
    sed -i 's/+l1UpgradeTfMuonTree/#+l1UpgradeTfMuonTree/' L1Trigger/L1TNtuples/python/L1NtupleRAW_cff.py
    sed -i 's/+l1UpgradeTfMuonShowerTree/#+l1UpgradeTfMuonShowerTree/' L1Trigger/L1TNtuples/python/L1NtupleRAW_cff.py
    
    git clone https://github.com/cms-hcal-trigger/Validation.git HcalTrigger/Validation

    scram b clean
    scram b -j8

    git clone https://github.com/HcalConditionsAutomatization/ConditionsValidation ConditionsValidation
}

function hlt_validation(){
    echo "Setting up hlt_validation area"

    scram b clean
    scram b -j8
}

function create_job_area(){
    echo "${@:2}"

    local setup_function=$1
    local cmssw_version=$2
    local cmssw_arch=$3

    printf "Setting up job area %s with %s with arch %s\n" $setup_function $cmssw_version $cmssw_arch 
    printf "Moving to home directory %s\n" $HCAL_WORKING_AREA

    echo "Initalizing cmssw area using"
    cd $HCAL_WORKING_AREA
    scram -a $cmssw_arch project $cmssw_version

    cd $cmssw_version/src
    cmsenv

    echo "Using archietcture $SCRAM_ARCH"
    cmssw_arch=$SCRAM_ARCH

    echo "Calling setup function"
    cd $CMSSW_BASE/src
    export HOME=$HCAL_WORKING_AREA

    git config --global user.name "HCALSERV"
    git config --global user.email "hcalserv@cern.ch"
    git config --global user.github "hcalserv"

    git cms-init

    $setup_function "${@:2}"
    setupPythonEnvironment

    cd $HCAL_WORKING_AREA

    cat << EOF >> $HCAL_WORKING_AREA/setup.sh
    #!/bin/bash
    source /cvmfs/cms.cern.ch/cmsset_default.sh
    cd $cmssw_version/src
    scramv1 b ProjectRename
    cmsenv
EOF
    ls -l
    pwd

    echo "PRODUCING TAR ENVIRONMENT"
    tar -czf "${setup_function}.tar.gz" --exclude=${cmssw_version}/src/hcalcalib/[.HP]* "${cmssw_version}" "setup.sh"

    echo "MOVING TAR ENVIRONMENT to /eos/user/h/hcalserv/automation_working_area/"
    mv "${setup_function}.tar.gz"  /eos/user/h/hcalserv/automation_working_area/
}

create_job_area "$@"
