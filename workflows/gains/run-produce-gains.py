from ecalautoctrl import process_by_fill
from hcal_condor import HCALCondorHandler
from hcal_oms import HCALQueryOMS
from hcal_runtools import allowGroupOverride

import sys


@allowGroupOverride
@process_by_fill(fill_complete=False)
class GainsProductionHandler(HCALCondorHandler):
    def __init__(self, task_name, script_path, **kwargs):
        super().__init__(task_name, script_path, **kwargs)

        self.augmentParser(self.submit_parser)
        self.submit_parser.add_argument("--raddam-model-HE", type = str, default = None)
        self.submit_parser.add_argument("--raddam-model-HF", type = str, default = None)
        self.submit_parser.add_argument("--initial-gains",   type = str, default = None)

        # Only care about doing gains projection for runs where pedestals produced
        self.prevTaskNames = kwargs["deps_task"]

    def getWorkItems(self, runObj):
        self.reconfigMMlogger()

        fillNumber = runObj["fill"]
        runNumber  = runObj["run_number"]

        omq = HCALQueryOMS(logger = self.log)

        boilerPlateMessage = f"OMS query for \"__ITEM__\" returned nothing, cannot proceed with task \"{self.task}\"."

        # Get the start time of the run in question
        runsInfo = omq.getFromOMS(
            item    = "runs",
            filters = HCALQueryOMS.make_filter([("run_number", "EQ", runNumber)]),
            attrs   = ["start_time"],
        )
        if not runsInfo:
            self.log.error(boilerPlateMessage.replace("__ITEM__", "runs"))
            return None, self.reprocessTaskStatus

        # Based on filter, only one run is returned
        runInfo = runsInfo[0]

        # Get the year for the run based on the start time stamp
        yearForRun = int(runInfo["start_time"].split("-")[0])

        # Query OMS for delivered lumi per year (here always start from beginning of Run3 i.e. 2022
        yearlyLumisInfo = omq.getFromOMS(
            item    = "yearlyluminosity",
            filters = HCALQueryOMS.make_filter([("year", "GT", 2021), ("year", "LT", yearForRun)]),
            attrs   = ["delivered_lumi"],
        )
        if not yearlyLumisInfo:
            self.log.error(boilerPlateMessage.replace("__ITEM__", "yearlyluminosity"))
            return None, self.reprocessTaskStatus

        # Add up all the delivered lumis from previous years not including the year for the current run
        totalAccuLumi = 0.0
        for yearlyLumiInfo in yearlyLumisInfo:
            totalAccuLumi += float(yearlyLumiInfo["delivered_lumi"])

        # Now get the accumulated delivered lumi from the start of the year of the run to start of run itself
        lumiSummaries = omq.getFromOMS(
            item    = "lumisummaries",
            filters = HCALQueryOMS.make_filter([("start_time", "GT", f"{yearForRun}-01-01T00:00:01Z"), ("start_time", "LT", runInfo["start_time"])]),
            attrs   = ["delivered_lumi"],
        )
        if not lumiSummaries:
            self.log.error(boilerPlateMessage.replace("__ITEM__", "lumisummaries"))
            return None, self.reprocessTaskStatus

        # This query will likely return multiple summaries, which are not orthogonal...
        # So take the one with the maximum delivered lumi as the "best" one...
        deliveredLumis = [float(lumiSummary["delivered_lumi"]) for lumiSummary in lumiSummaries]
        totalAccuLumi += max(deliveredLumis)

        # Numbers from OMS are in pb^-1, so convert to fb^-1 for raddam evaluation
        totalAccuLumi /= 1000.0

        self.log.info(f"Will extrapolate gains for a total accumulated luminosity of {totalAccuLumi:.3f} /fb for {self.runAndFill}.")

        args = [{"arguments": f"{totalAccuLumi} {self.opts.raddam_model_HE} {self.opts.raddam_model_HF} {self.opts.initial_gains} {self.opts.prev_gains_dir}"}]

        return args, None



if __name__ == "__main__":

    handler = GainsProductionHandler("gains-production", "scripts/run-produce-gains.sh", deps_task = None)
    ret_code = handler()
    sys.exit(ret_code)
