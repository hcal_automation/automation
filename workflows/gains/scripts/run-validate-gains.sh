# First three arguments are always task name e.g. make-ntuples, job id related to condor job, and directory
# on EOS in the form /eos/cms/store/group/dpg_hcal/comm_hcal/AUTOMATION_TEST_AREA/${TASK}/${RUN}
TASK=${1}
JOB_ID=${2}
EOS_DIR=${3}
PUBLIC_DIR=${4}

# Gains tar containing txt files for gains
GAINS_TAR=${5}
RUN_NUMBER=${6}
PREV_GAINS_DIR=${7}

print_args TASK JOB_ID EOS_DIR PUBLIC_DIR GAINS_TAR RUN_NUMBER PREV_GAINS_DIR

ecalautomation.py ${TASK} jobctrl --id ${JOB_ID} --running

cd ${CMSSW_BASE}/src
cd hcalcalib/AutomationScripts
execute "tar -xzvf ${GAINS_TAR}"
echo -e "\n\n"

# Get the run numbers of the old and new runs
PREV_RUN_NUMBER=`cat ${PREV_GAINS_DIR}/run_number.txt`

OUTPUT_NAME="gains_${RUN_NUMBER}_from_${PREV_RUN_NUMBER}"

# Run the validation plots using the old and new pedestal tables, and use the run numbers in the output filenames
execute "./ValidateGainPed ./Gains.txt ${PREV_GAINS_DIR}/Gains.txt ${OUTPUT_NAME}"
echo -e "\n\n"

execute "cp gains*.{root,pdf} ${EOS_DIR}"

# Move pdfs to folder for web viewing
execute "cp gains*.pdf ${PUBLIC_DIR}"

ecalautomation.py ${TASK} jobctrl --done --id ${JOB_ID} --fields "output:${EOS_DIR}/${OUTPUT_NAME}.root"
