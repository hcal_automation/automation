# First three arguments are always task name e.g. make-ntuples, job id related to condor job, and directory
# on EOS in the form /eos/cms/store/group/dpg_hcal/comm_hcal/AUTOMATION_TEST_AREA/${TASK}/${RUN}
TASK=${1}
JOB_ID=${2}
EOS_DIR=${3}

EVAL_LUMI=${4}
RADDAM_MODEL_HE=${5}
RADDAM_MODEL_HF=${6}
INITIAL_GAINS_TXT=${7}
PREV_GAINS_DIR=${8}

print_args TASK JOB_ID EOS_DIR EVAL_LUMI RADDAM_MODEL_HE RADDAM_MODEL_HF INITIAL_GAINS_TXT PREV_GAINS_DIR

ecalautomation.py ${TASK} jobctrl --id ${JOB_ID} --running

cd ${CMSSW_BASE}/src

# Get updates for R. Taus framework
execute "cd radiation-damage-reader"
execute "git pull origin master"

execute "cd .."

# Get updates for gains converter framework
execute "cd gains"
execute "git pull origin master"
execute "make -j4"

GAINS_TXT="Gains.txt"
GAINS_TAR="gains.tar.gz"

PREV_GAINS_TXT="Gains_prev.txt"
NEW_GAINS_HF_TXT="Gains_new_HF.txt"
NEW_GAINS_HEHF_TXT="Gains_new_HEHF.txt"

echo -e "\n\n"

# Project gains and write to txt files
echo "Running HF and HE gains converter and writing to text files"
# Run laser analysis and pass lumi number
execute "./HFGainConverterParam init_gains/${INITIAL_GAINS_TXT} raddam_files/${RADDAM_MODEL_HF} 0.0 ${EVAL_LUMI} ${NEW_GAINS_HF_TXT}"
execute "./HEGainConverterParam ${NEW_GAINS_HF_TXT} raddam_files/${RADDAM_MODEL_HE} 0.0 ${EVAL_LUMI} ${NEW_GAINS_HEHF_TXT}"

echo -e "\n\n"

# Copy the previous gains to copy latest HB, HF, etc. from
execute "cp ${PREV_GAINS_DIR}/${GAINS_TXT} ${PREV_GAINS_TXT}"

construct_gains_txt ${PREV_GAINS_TXT} ${NEW_GAINS_HEHF_TXT} ${GAINS_TXT}

echo "Doing redump of text files with CMSSW to reformat"
execute "mv ${GAINS_TXT} tempGains.txt"
reformat_conditions_txt Gains gains/tempGains.txt ${GAINS_TXT}
echo -e "\n\n"

# Mimic pedestals workflow and tar up gains txt file for storage
execute "tar -cvzf ${GAINS_TAR} ${GAINS_TXT}"
execute "cp ${GAINS_TAR} ${EOS_DIR}"

ecalautomation.py ${TASK} jobctrl --done --id ${JOB_ID} --fields "gains:${EOS_DIR}/${GAINS_TAR}"
