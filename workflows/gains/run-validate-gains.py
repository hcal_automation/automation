from ecalautoctrl import process_by_fill
from hcal_condor import HCALCondorHandler
from hcal_runtools import allowGroupOverride

import sys


@allowGroupOverride
@process_by_fill(fill_complete=False)
class GainsValidationHandler(HCALCondorHandler):
    def __init__(self, task_name, script_path, **kwargs):
        super().__init__(task_name, script_path, **kwargs)

        self.augmentParser(self.submit_parser)

        self.prevTaskNames = kwargs["deps_task"]


    def getWorkItems(self, runObj):
        self.reconfigMMlogger()

        return self.getPreviousTaskOutput(runObj, fieldName = "gains")



if __name__ == "__main__":

    handler = GainsValidationHandler("gains-validation", "scripts/run-validate-gains.sh", deps_task = ["gains-production"])
    ret_code = handler()
    sys.exit(ret_code)
