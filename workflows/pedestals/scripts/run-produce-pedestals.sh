# First three arguments are always task name e.g. make-ntuples, job id related to condor job, and directory
# on EOS in the form /eos/cms/store/group/dpg_hcal/comm_hcal/AUTOMATION_TEST_AREA/${TASK}/${RUN}
TASK=${1}
JOB_ID=${2}
EOS_DIR=${3}

# RAW file from TestEnables RAW data set
RAW_FILE=${4}
RUN_NUMBER=${5}
PREV_PEDESTALS_DIR=${6}

print_args TASK JOB_ID EOS_DIR RAW_FILE RUN_NUMBER PREV_PEDESTALS_DIR

ecalautomation.py ${TASK} jobctrl --id ${JOB_ID} --running

cd ${CMSSW_BASE}/src

HCALNANO_OUTPUT_FILE="hcalnano_${JOB_ID}.root"

CMS_DRIVER_CMD="""
cmsDriver.py NANO \
    -s RAW2DIGI,RECO,USER:DPGAnalysis/HcalNanoAOD/hcalNano_cff.hcalNanoTask \
    --datatier NANOAOD \
    --eventcontent NANOAOD \
    --processName USER \
    --filein ${RAW_FILE} \
    --fileout ${HCALNANO_OUTPUT_FILE} \
    --conditions auto:run3_data_prompt \
    -n 3000 \
    --nThreads ${NCPUS} \
    --era Run3 \
    --customise DPGAnalysis/HcalNanoAOD/customiseHcalCalib_cff.customiseHcalCalib \
    --python_filename test_global_cfg.py \
    --no_exec
"""

echo "Running cmsDriver command:"
echo "${CMS_DRIVER_CMD}"
echo -e "\n\n"

execute "${CMS_DRIVER_CMD}"
echo -e "\n\n"

echo "Generated cmsRun config is:"
cat test_global_cfg.py
echo -e "\n\n"

execute "cmsRun test_global_cfg.py"

###############################################################################################################
# We have made an HCAL Nano file, so process it and measure the pedestals

# Dynamically make task dir and tmp subdir
mkdir -p ${EOS_DIR}/tmp

# Check out the pedestals subframework within the automation area
execute "cd pedestals"
execute "git pull origin dev"
echo -e "\n\n"

# For reformatting conds down below without moving files around
CMSSW_SEARCH_PATH=${PWD}:${CMSSW_SEARCH_PATH}

PEDESTALS_TXT="EffectivePedestals.txt"
PEDESTALWIDTHS_TXT="EffectivePedestalWidths.txt"

PREV_PEDESTALS_TXT="${PEDESTALS_TXT}.prev"
PREV_PEDESTALWIDTHS_TXT="${PEDESTALWIDTHS_TXT}.prev"

NEW_PEDESTALS_TXT="${PEDESTALS_TXT}.new"
NEW_PEDESTALWIDTHS_TXT="${PEDESTALWIDTHS_TXT}.new"

# Get the previous pedestals txt files, which ZDC will be taken from (since ZDC not covered here)
execute "cp ${PREV_PEDESTALS_DIR}/${PEDESTALS_TXT} ${PREV_PEDESTALS_TXT}"
execute "cp ${PREV_PEDESTALS_DIR}/${PEDESTALWIDTHS_TXT} ${PREV_PEDESTALWIDTHS_TXT}"

# Process hcalnano and write calculated pedestals to txt files
echo "Processing HcalNano ROOT file, deriving pedestals, and writing to text files"
execute "./nano_to_digi ../${HCALNANO_OUTPUT_FILE} ${RUN_NUMBER}"
echo -e "\n\n"

# Write pedestal trends
echo "Calculating pedestal trends"
execute "python3 CalculatePedestalTrends.py ${EOS_DIR}"
execute "source makePlots.sh"
echo -e "\n\n"

construct_pedestals_txt PedestalTable.txt ${PREV_PEDESTALS_TXT} ${NEW_PEDESTALS_TXT}
construct_pedestals_txt PedestalTableWidth.txt ${PREV_PEDESTALWIDTHS_TXT} ${NEW_PEDESTALWIDTHS_TXT}

echo "Doing redump of text files with CMSSW to reformat"
reformat_conditions_txt EffectivePedestals ${NEW_PEDESTALS_TXT} ${PEDESTALS_TXT}
echo -e "\n\n"
reformat_conditions_txt EffectivePedestalWidths ${NEW_PEDESTALWIDTHS_TXT} ${PEDESTALWIDTHS_TXT}
echo -e "\n\n"

PEDESTALS_TAR=pedestals.tar.gz
ZS_XML=output.xml

# Move the ZS XML file to a temp directory so XMLDB cron job doesn't pick up on it before we want
execute "tar -czf ${PEDESTALS_TAR} ${ZS_XML} output.root ${PEDESTALS_TXT} ${PEDESTALWIDTHS_TXT} Table.txt"
execute "cp ${PEDESTALS_TAR} ${EOS_DIR}"
execute "cp ${ZS_XML} ${EOS_DIR}/tmp"
execute "cp ${CMSSW_BASE}/src/${HCALNANO_OUTPUT_FILE} ${EOS_DIR}"

ecalautomation.py ${TASK} jobctrl --done --id ${JOB_ID} --fields "pedestals:${EOS_DIR}/${PEDESTALS_TAR}" "zs_file:${EOS_DIR}/tmp/${ZS_XML}"
