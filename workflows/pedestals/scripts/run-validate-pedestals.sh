# First three arguments are always task name e.g. pedestals-production, job id related to condor job, and directory
# on EOS in the form /eos/cms/store/group/dpg_hcal/comm_hcal/AUTOMATION_TEST_AREA/${TASK}/${RUN}
TASK=${1}
JOB_ID=${2}
EOS_DIR=${3}
PUBLIC_DIR=${4}

# Pedestals tar containing txt files for pedestals and pedestal widths
PEDESTALS_TAR=${5}
RUN_NUMBER=${6}
PREV_PEDESTALS_DIR=${7}

print_args TASK JOB_ID EOS_DIR PUBLIC_DIR PEDESTALS_TAR RUN_NUMBER PREV_PEDESTALS_DIR

ecalautomation.py ${TASK} jobctrl --id ${JOB_ID} --running

cd ${CMSSW_BASE}/src
cd hcalcalib/AutomationScripts
execute "tar -xzvf ${PEDESTALS_TAR}"
echo -e "\n\n"

# Get the run numbers of the previous run of pedestals
PREV_RUN_NUMBER=`cat ${PREV_PEDESTALS_DIR}/run_number.txt`

VAL_OUTPUT_NAME="pedestals_${RUN_NUMBER}_from_${PREV_RUN_NUMBER}"

# Run the validation plots using the old and new pedestal tables, and use the run numbers in the output filenames
execute "./ValidateGainPed ./EffectivePedestals.txt ${PREV_PEDESTALS_DIR}/EffectivePedestals.txt ${VAL_OUTPUT_NAME}"
echo -e "\n\n"
execute "./ValidateGainPed ./EffectivePedestalWidths.txt ${PREV_PEDESTALS_DIR}/EffectivePedestalWidths.txt ${VAL_OUTPUT_NAME/pedestals/pedestal_widths}"
echo -e "\n\n"

execute "cp pedestal*.{root,pdf} ${EOS_DIR}"

execute "cp pedestal*.pdf ${PUBLIC_DIR}"

ecalautomation.py ${TASK} jobctrl --done --id ${JOB_ID} --fields "output:${EOS_DIR}/${VAL_OUTPUT_NAME}.root"
