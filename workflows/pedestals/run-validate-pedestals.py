from ecalautoctrl import process_by_fill
from hcal_condor import HCALCondorHandler
from hcal_runtools import allowGroupOverride

import sys


@allowGroupOverride
@process_by_fill(fill_complete=False)
class PedestalsValidationHandler(HCALCondorHandler):
    def __init__(self, task_name, script_path, **kwargs):
        super().__init__(task_name, script_path, **kwargs)

        self.augmentParser(self.submit_parser)

        self.prevTaskNames = kwargs["deps_task"]


    def getWorkItems(self, runObj):
        self.reconfigMMlogger()

        return self.getPreviousTaskOutput(runObj, fieldName = "pedestals")



if __name__ == "__main__":

    handler = PedestalsValidationHandler("pedestals-validation", "scripts/run-validate-pedestals.sh", deps_task = ["pedestals-production"])
    ret_code = handler()
    sys.exit(ret_code)
