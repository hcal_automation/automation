from ecalautoctrl import process_by_fill
from hcal_condor import HCALCondorHandler
from hcal_oms import HCALQueryOMS
from hcal_runtools import allowGroupOverride

import sys
import numpy as np
from datetime import datetime, timezone


@allowGroupOverride
@process_by_fill(fill_complete=False)
class PedestalsProductionHandler(HCALCondorHandler):

    def __init__(self, task_name, script_path, **kwargs):
        super().__init__(task_name, script_path, **kwargs)

        self.augmentParser(self.submit_parser)
        self.submit_parser.add_argument("--lumi-for-pedestals", type = str)

        # Name of test enables data set to retrieve RAW ROOT files from
        # Determined below by what is passed from Jenkins workflow GUI
        self.dataSetName = None

        # The pedestal ntuple production task is the very first in the chain
        # by definition there is no previous task for it to rely on
        self.prevTaskNames = kwargs["deps_task"] # i.e. None

        # Use events taken during this instantaneous lumi value (Hz/ub)
        # Defined later by param from Jenkins GUI passed via --lumi-for-pedestals
        self.lumiForPedestals = None

        # Allowed +/- threshold for lumi relative to target lumi for pedestals measurement
        self.lumiForPedestalsPrecision = 0.05


    def checkIfGoodFill(self, hcalOMSqueryImpl, aFillObj):

        # Helper function to make a unix time float from a string time stamp
        def stamp2unix(theTime):
            strFormat = "%Y-%m-%dT%H:%M:%SZ"
            if "." in theTime:
                strFormat = strFormat.replace("Z", ".%fZ")

            timeObj = datetime.strptime(theTime, strFormat)

            return timeObj.replace(tzinfo = timezone.utc).timestamp()

        theFill = aFillObj["fill_number"]

        sbStartTime = aFillObj["start_stable_beam"]
        sbEndTime   = aFillObj["end_stable_beam"]
    
        # Get all lumi points corresponding to a fill
        lumiObjs = hcalOMSqueryImpl.getLumiInfo(
            start_time = sbStartTime,
            end_time   = sbEndTime
        )

        if len(lumiObjs) == 0:
            self.log.info(f"Fill {theFill} does not have any measured instantaneous luminosity points.")
            return False, None, None

        # Define a time (UNIX) that is clearly beyond the start of stable beams
        # E.g. a time 90 minutes after stable beams are declared should be into the lumi leveling period
        # This time is used later to veto lumi points that might have the target lumi for pedestals measurement
        # but are from the very beginning of stable beams
        postStartStableBeamTime = stamp2unix(sbStartTime) + 3600. * 1.5

        # From OMS' returned list of dicts, make a list of lists for slicing and selecting
        lumiAttrList = []
        pedestalsLumiTime = -1
        pedestalsLumiDiff = 1e10
        for lumiObj in lumiObjs:
            unixDipTime = stamp2unix(lumiObj["dip_time"])
            lumiAttrList.append([float(lumiObj["inst_lumi"]), unixDipTime, int(lumiObj["run"])])

            # Keep track of the time for the lumi point closest to the target lumi for pedestals
            if abs(lumiAttrList[-1][0] - self.lumiForPedestals) < pedestalsLumiDiff and \
               lumiAttrList[-1][1] > postStartStableBeamTime:
                pedestalsLumiDiff = abs(lumiAttrList[-1][0] - self.lumiForPedestals)
                pedestalsLumiTime = lumiAttrList[-1][1]
        
        # Numpy-ify the list of attribute tuples
        lumiAttrArray = np.array(lumiAttrList)
        
        # Define a 1 hour window centered around the time when the target instantaneous luminosity occurred
        pedestalsLumiStartTime = pedestalsLumiTime - 3600. * 0.5
        pedestalsLumiEndTime   = pedestalsLumiTime + 3600. * 0.5
       
        # First make a mask for lumi points below the ideal lumi value 
        # Make a selection mask for lumi points after the start of lumi decay
        # Avoid being "tricked" by large drops in lumi at the end of the run
        targetLumiRangeMask = (lumiAttrArray[:,1]>pedestalsLumiStartTime) & \
                              (lumiAttrArray[:,1]<pedestalsLumiEndTime)
        
        # Check first that during the lumi decay period, we actually reached the ideal lumi value
        lumiVals = lumiAttrArray[targetLumiRangeMask][:,0]
        if len(lumiVals) == 0:
            self.log.info(f"Did not reach target instantaneous luminosity of {self.lumiForPedestals} Hz/ub following lumi-leveling (possibly early dump) during fill {theFill}.")
            return False, None, None

        # Get the list of downtimes for HCAL and make unix time list
        daqTimeObjs = hcalOMSqueryImpl.getDAQstatus(scope = "fill", number = theFill)
        daqAttrList = []
        for daqTimeObj in daqTimeObjs:
            startTimeUnix = stamp2unix(daqTimeObj["start_time"])
            stopTimeUnix  = stamp2unix(daqTimeObj["stop_time"])
            daqAttrList.append([startTimeUnix, stopTimeUnix])
        daqAttrArray = np.array(daqAttrList)

        # Count number of HCAL downtimes that occur during golden lumi period
        # Consider a one hour window right before and during the target instantaneous lumi
        # for pedestals measurement and ensure no HCAL downtimes, no excessive run starts, 
        # no fluctuations in lumi, etc.

        if len(daqAttrArray) != 0:
            daqDownStartTimeMask = (daqAttrArray[:,0]>pedestalsLumiStartTime) & \
                                   (daqAttrArray[:,0]<pedestalsLumiEndTime)
        
            daqDownStopTimeMask = (daqAttrArray[:,1]>pedestalsLumiStartTime) & \
                                  (daqAttrArray[:,1]<pedestalsLumiEndTime)

            daqDownFullTimeMask = (daqAttrArray[:,0]<pedestalsLumiStartTime) & \
                                  (daqAttrArray[:,1]>pedestalsLumiEndTime)

            numberDownTimes = (daqDownStartTimeMask|daqDownStopTimeMask|daqDownFullTimeMask).sum()
            if numberDownTimes > 0:
                self.log.info(f"HCAL downtimes near/during time of pedestals measurement for fill {theFill}.")
                return False, None, None

        # This golden zone is the ~1 hour window during which the target instantaneous lumi is reached
        goldenLumiMask = (lumiAttrArray[:,1]>pedestalsLumiStartTime) & \
                         (lumiAttrArray[:,1]<pedestalsLumiEndTime)
        
        # Get runs that occurred during lumi decay
        runs = np.unique(lumiAttrArray[goldenLumiMask][:,2])
        
        # Any more than two runs during time window for pedestals measurement is probably problematic
        if len(runs) > 1:
            self.log.info(f"Excessive number of runs ({len(runs)}) around time of pedestals measurement in fill {theFill}.")
            return False, None, None
        
        # Detect inst. lumi (Hz/ub) outliers (drops) during time window during which pedestals would be measured
        lumiVals = lumiAttrArray[goldenLumiMask][:,0]
        if len(lumiVals) > 0 and np.min(lumiVals) < 0.5 * self.lumiForPedestals:
            self.log.info(f"Large instantaneous luminosity fluctuations (drops) around time of pedestals measurement from {np.min(lumiVals):.2e} to {np.max(lumiVals):.2e} Hz/ub (target instantaneous luminosity {self.lumiForPedestals:.2e}) in fill {theFill}.")
            return False, None, None

        self.log.info(f"Fill {theFill} passed initial quality checks and is candidate for pedestals measurement.")
        return True, datetime.fromtimestamp(pedestalsLumiStartTime, tz = timezone.utc).strftime('%Y-%m-%dT%H:%M:%S.%fZ'), datetime.fromtimestamp(pedestalsLumiEndTime, tz = timezone.utc).strftime('%Y-%m-%dT%H:%M:%S.%fZ')


    def getWorkItems(self, runObj):
        # Set the logging format for mattermost messages
        self.reconfigMMlogger()

        # This is actually the fill number we care about, corresponding to the latest run in the group
        fillNumber = runObj["fill"]
        runNumber  = runObj["run_number"]

        # Comes from the Jenkins GUI
        self.dataSetName      = self.opts.dataset
        self.lumiForPedestals = float(self.opts.lumi_for_pedestals)
        self.runForFiles      = runNumber

        # For the passed run object trivially get the fill covering that run
        omq = HCALQueryOMS(logger = self.log)

        fillAsList = [fillNumber]

        # If we are doing cosmics, then do not need to worry about stable beams or not
        stableBeamsFilter = None
        if not self.useCosmics:
            stableBeamsFilter = HCALQueryOMS.make_filter([("stable_beams", "EQ", "true"), ("fill_number", "EQ", fillNumber)])

        fills = omq.getFromOMS(
            item    = "fills",
            attrs   = ["fill_number", "start_stable_beam", "end_stable_beam"],
            filters = stableBeamsFilter,
        )
        if not fills:
            self.log.info(f"OMS query for \"fills\" {fillAsList[0]} returned nothing (possible no stable beams), will not process current run group (fill) any further.")
            return None, self.ineligibleFillTaskStatus

        # At most only one fill will be in the list by construction
        fillObj = fills[0]

        # Depending on if we want pedestals from a cosmics run or a collisions run,
        # we may or may not need to check for the same things
        lumiObjs = None
        sortedLumis = []
        if not self.useCosmics:
            # Check if the fill is "good" for pedestals measurement following
            # the criteria outlined in https://twiki.cern.ch/twiki/bin/viewauth/CMS/HcalAutomator
            # Note that this only applied when measuring pedestals during collisions
            isGoodFill, pedestalsLumiStartTime, pedestalsLumiEndTime = self.checkIfGoodFill(omq, fillObj)
            if not isGoodFill:
                return None, self.ineligibleFillTaskStatus

            # The fill is good, so now grab all lumi points near the target inst lumi value
            lumiObjs = omq.getLumiInfo(
                run_number = runNumber,
                start_time = pedestalsLumiStartTime,
                end_time   = pedestalsLumiEndTime
            )

            # For collisions, the lumiObjs from getLumiInfo will include LS objects
            # and so we sort those objects by how close their instantaneous luminosity
            # is to the ideal value self.lumiForPedestals
            # Also use the lumiForPedestalsPrecision to ensure candidate LS are very close to target
            if lumiObjs:
                tempLumiObjs = sorted(lumiObjs, key=lambda x: abs(x["inst_lumi"] - self.lumiForPedestals) / self.lumiForPedestals)
                for lumiObj in tempLumiObjs:
                    if abs(lumiObj["inst_lumi"] - self.lumiForPedestals) / self.lumiForPedestals <= self.lumiForPedestalsPrecision:
                        sortedLumis.append(lumiObj["lumi_section"])

        # Here we want do things specifically for cosmics runs
        else:
            lumiObjs = omq.getLumiInfo(
                useCosmics = self.useCosmics,
                run_number = runNumber
            )

            # For cosmics, the lumiObjs for a run is just the list of subdets that were IN
            # and the number for the last LS. In this case, any LS could be used, so just
            # construct a simple list from 0 to last LS
            if lumiObjs:
                # Make sure HCAL was actually in the run
                includedSubdetectors = lumiObjs[0]["components"]
                lastLumiSectionOfRun = lumiObjs[0]["last_lumisection_number"] 
                if "HCAL" in includedSubdetectors:
                    for lumiSection in range(1, int(lastLumiSectionOfRun)+1):
                        sortedLumis.append(lumiSection)
                else:
                    self.log.info(f"HCAL was not included in global for cosmics run {runNumber}.")
            else:
                self.log.info(f"Considered {self.runAndFill} is not a cosmics run.")
                return None, self.ineligibleRunTaskStatus

        # Cannot proceed further if there are no candidate LS to process
        if not sortedLumis:
            self.log.info(f"No lumi sections close enough to target instantaneous luminosity of {self.lumiForPedestals:.2e} Hz/ub were identified for {self.runAndFill}.")
            return None, self.ineligibleRunTaskStatus

        # Now try and get the RAW ROOT files that correspond to the run and LS
        # The first found ROOT file will return out, via the above sorting, this file
        # will contains the LS closest to the target lumi as possible
        for ls in sortedLumis:
            # Lumi sections are not split across files, so "files" here is a single-element list
            rawFiles = self.getFilesDBS(lumiList=[ls])

            if rawFiles:
                args = [{"arguments": f"{rawFile} {runNumber} {self.opts.prev_pedestals_dir}"} for rawFile in rawFiles]
                return args, None

        # No ROOT files could be obtained for the relevant LS, so return nothing
        self.log.warning(f"Could not retrieve any RAW files from data set \"{self.dataSetName}\" for {self.runAndFill} while there are candidate lumi sections for processing.")

        return None, self.reprocessTaskStatus




if __name__ == "__main__":

    handler = PedestalsProductionHandler("pedestals-production", "scripts/run-produce-pedestals.sh", deps_task = None)
    ret_code = handler()
    sys.exit(ret_code)
