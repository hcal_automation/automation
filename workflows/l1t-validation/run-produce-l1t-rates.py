from hcal_condor import HCALCondorHandler
from hcal_runtools import allowGroupOverride
from ecalautoctrl import process_by_fill

import sys
import glob


@allowGroupOverride
@process_by_fill(fill_complete=False)
class L1TRatesProductionHandler(HCALCondorHandler):
    def __init__(self, task_name, script_path, **kwargs):
        super().__init__(task_name, script_path, **kwargs)

        self.augmentParser(self.submit_parser)

        self.prevTaskNames = kwargs["deps_task"]

    def getWorkItems(self, runObj):
        self.reconfigMMlogger()
        runNumber = runObj["run_number"]

        # Get all the ROOT files output by the l1-rate task for when running with "new" conditions
        # Output files for when running on "old" conditions will be retrieved in the shell script
        l1tNtupleFilesPath = f"{self.opts.eosdir}/{self.prevTaskNames[0]}/{runNumber}/new/*.root"
        l1tNtupleFiles = glob.glob(l1tNtupleFilesPath)

        args = [{"arguments": file} for file in l1tNtupleFiles]            

        return args, None



if __name__ == "__main__":

    handler = L1TRatesProductionHandler("l1t-rates-production", "scripts/run-produce-l1t-rates.sh", deps_task = ["l1t-ntuples-production"])
    ret_code = handler()
    sys.exit(ret_code)
