from ecalautoctrl import process_by_fill, JobCtrl
from hcal_condor import HCALCondorHandler
from hcal_runtools import allowGroupOverride

import sys


@allowGroupOverride
@process_by_fill(fill_complete=False)
class L1TNtuplesProductionHandler(HCALCondorHandler):
    def __init__(self, task_name, script_path, **kwargs):
        super().__init__(task_name, script_path, **kwargs)

        self.augmentParser(self.submit_parser)

        self.prevTaskNames = kwargs["deps_task"]

        self.dataSetName = None
        self.runForFiles = None

    def getWorkItems(self, runObj):
        self.reconfigMMlogger()

        runNumber  = runObj["run_number"]
        fillNumber = runObj["fill"]

        # Comes from the Jenkins GUI
        self.dataSetName = self.opts.dataset
        self.runForFiles = self.opts.val_run

        # Get the new LUT tag and L1 trigger objects DB from the lut-production task
        lut_task = JobCtrl(
            task=self.prevTaskNames[0],
            campaign=self.opts.campaign[0],
            tags={"run_number": runNumber, "fill": fillNumber},
            dbname=self.opts.dbname,
        )
        # The zeroth of two jobs for lut-production is for the new conditions
        # The old conditions are inferred in the sh script
        new_lut_job = lut_task.getJob(0, last=True)[0]
        ref_lut_job = lut_task.getJob(1, last=True)[0]

        new_l1trgobjs_db = new_lut_job["l1_trg_objs_db"]
        ref_l1trgobjs_db = ref_lut_job["l1_trg_objs_db"]

        # Get the listing of RAW files corresponding to the dataSetName
        rawFiles = self.getFilesDBS()
        if not rawFiles:
            self.log.error(f"Could not retrieve any valid RAW files from data set \"{self.dataSetName}\" for reference run {self.runForFiles}. Review the requested data set name, and try running this workflow again.")
            return None, self.reprocessTaskStatus

        args = [{"arguments": f"{self.opts.global_tag} {file} {new_l1trgobjs_db} {ref_l1trgobjs_db}"} for idx, file in enumerate(rawFiles) if idx < self.opts.max_input_files]

        return args, None



if __name__ == "__main__":

    handler = L1TNtuplesProductionHandler("l1t-ntuples-production", "scripts/run-produce-l1t-ntuples.sh", deps_task = ["lut-production"])
    ret_code = handler()
    sys.exit(ret_code)
