from hcal_condor import HCALCondorHandler
from hcal_runtools import allowGroupOverride
from ecalautoctrl import process_by_fill

import sys


@allowGroupOverride
@process_by_fill(fill_complete=False)
class L1TRatesValidationHandler(HCALCondorHandler):
    def __init__(self, task_name, script_path, **kwargs):
        super().__init__(task_name, script_path, **kwargs)

        self.augmentParser(self.submit_parser)

        self.prevTaskNames = kwargs["deps_task"]


    def getWorkItems(self, runObj):
        self.reconfigMMlogger()

        runNumber = runObj["run_number"]

        args = [{"arguments": f"{self.opts.eosdir}/{self.prevTaskNames[0]}/{runNumber} {runNumber}"}]

        return args, None



if __name__ == "__main__":

    handler = L1TRatesValidationHandler("l1t-rates-validation", "scripts/run-validate-l1t-rates.sh", deps_task = ["l1t-rates-production"])
    ret_code = handler()
    sys.exit(ret_code)
