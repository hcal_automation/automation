#!/usr/bin/env bash

# First three arguments are always task name e.g. make-ntuples, job id related to condor job, and directory
# on EOS in the form /eos/cms/store/group/dpg_hcal/comm_hcal/AUTOMATION_TEST_AREA/${TASK}/${RUN}
TASK=${1}
JOB_ID=${2}
EOS_DIR=${3}

L1NTUPLES_FILE_NEWCONDS=${4}

print_args TASK JOB_ID EOS_DIR L1NTUPLES_FILE_NEWCONDS

ecalautomation.py ${TASK} jobctrl --id ${JOB_ID} --running

cd ${CMSSW_BASE}/src/HcalTrigger/Validation/scripts

# L1 ntuples ROOT files have same name, just saved in either new or def subdirectory
L1NTUPLES_FILE_OLDCONDS="${L1NTUPLES_FILE_NEWCONDS//\/new\//\/def\/}"

function compute_rates() {
    NEW_OR_DEF=$1
    FILE=$2

    ls -lrt ${FILE}
    
    xrdcp -f root://eoscms.cern.ch/${FILE} .
    root_file="${FILE##*/}"
    echo -e "\n\n"
    
    rates.exe ${NEW_OR_DEF} ${root_file}
    echo -e "\n\n"
}

execute "compute_rates def ${L1NTUPLES_FILE_OLDCONDS}"
execute "compute_rates new ${L1NTUPLES_FILE_NEWCONDS}"

# Rate computation seems to have worked, so copying over files
RATETYPES=("Inclusive" "HB" "HE1" "HE2" "HF")
CONDS=("def" "new_cond")
for RATETYPE in ${RATETYPES[@]}; do
    RATETYPESTR=
    if [[ "${RATETYPE}" != "Inclusive" ]]; then
        RATETYPESTR="_${RATETYPE}"
    else
        RATETYPESTR=""
    fi

    mkdir -p ${EOS_DIR}/rates/${RATETYPE}

    for COND in ${CONDS[@]}; do
        execute "mv rates_${COND}${RATETYPESTR}.root ${EOS_DIR}/rates/${RATETYPE}/rates_${COND}_${JOB_ID}.root"
    done
done

ecalautomation.py ${TASK} jobctrl --id ${JOB_ID} --done
