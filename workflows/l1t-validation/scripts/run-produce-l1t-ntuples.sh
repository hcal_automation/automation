#!/usr/bin/env bash

# First three arguments are always task name e.g. make-ntuples, job id related to condor job, and directory
# on EOS in the form /eos/cms/store/group/dpg_hcal/comm_hcal/AUTOMATION_TEST_AREA/${TASK}/${RUN}
TASK=${1}
JOB_ID=${2}
EOS_DIR=${3}

GLOBAL_TAG=${4}
RAW_FILE=${5}
TRIGGER_OBJECTS_DB=${6}
TRIGGER_OBJECTS_DB_REF=${7}

print_args TASK JOB_ID EOS_DIR GLOBAL_TAG RAW_FILE TRIGGER_OBJECTS_DB TRIGGER_OBJECTS_DB_REF

cd ${CMSSW_BASE}/src

ecalautomation.py ${TASK} jobctrl --id ${JOB_ID} --running

# Make L1 ntuples either with reference conds or with new conds
function do_l1_rates {

    mkdir -p $2
    cd $2

    FILENAME="ntuple_maker_$2.py"

    cmsDriver.py l1Ntuple -s RAW2DIGI \
      --python_filename="${FILENAME}" \
      -n 5000 --no_output \
      --era=Run3 --data \
      --conditions="${GLOBAL_TAG}" \
      --customise=L1Trigger/Configuration/customiseReEmul.L1TReEmulFromRAWsimHcalTP \
      --customise=L1Trigger/L1TNtuples/customiseL1Ntuple.L1NtupleRAWEMU \
      --custom_conditions=Tag,HcalL1TriggerObjectsRcd,sqlite_file:$3 \
      --customise_commands='process.HcalTPGCoderULUT.LUTGenerationMode=cms.bool(False); process.MessageLogger.cerr.WARNING = dict(limit = 0)' \
      --filein=${RAW_FILE}\
      --fileout=L1Ntuple.root \
      --no_exec 
    
    echo "Created L1 ntuple maker file:"
    cat ${FILENAME}
    echo -e "\n\n"
    
    echo "Running ntuple maker for $2"
    execute "cmsRun ${FILENAME}"
    echo -e "\n\n"
    
    ls -lrt
    echo -e "\n\n"

    mkdir -p ${EOS_DIR}/$2
    mv L1Ntuple.root ${EOS_DIR}/$2/L1Ntuple_$1.root

}

do_l1_rates ${JOB_ID} def ${TRIGGER_OBJECTS_DB_REF}
do_l1_rates ${JOB_ID} new ${TRIGGER_OBJECTS_DB}

ecalautomation.py ${TASK} jobctrl --id ${JOB_ID} --done
