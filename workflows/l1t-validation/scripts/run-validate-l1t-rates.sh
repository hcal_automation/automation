#!/usr/bin/env bash

# First three arguments are always task name e.g. make-ntuples, job id related to condor job, and directory
# on EOS in the form /eos/cms/store/group/dpg_hcal/comm_hcal/AUTOMATION_TEST_AREA/${TASK}/${RUN}
TASK=${1}
JOB_ID=${2}
EOS_DIR=${3}
PUBLIC_DIR=${4}

# Path is to the rates ROOT files from the l1-histograms task
FILES_PATH=${5}
RUN_NUMBER=${6}

print_args TASK JOB_ID EOS_DIR PUBLIC_DIR FILES_PATH RUN_NUMBER

ecalautomation.py ${TASK} jobctrl --id ${JOB_ID} --running

cd ${CMSSW_BASE}/src/HcalTrigger/Validation/scripts

RATETYPES=("Inclusive" "HB" "HE1" "HE2" "HF")
for RATETYPE in ${RATETYPES[@]}; do

    mkdir ${RATETYPE}
    cd ${RATETYPE}
    mkdir plots

    execute "hadd rates_def.root ${FILES_PATH}/rates/${RATETYPE}/*def*root"
    execute "hadd rates_new_cond.root ${FILES_PATH}/rates/${RATETYPE}/*new*root"
    
    execute "draw_rates.exe"
    echo -e "\n\n"

    PLOTS_DIR="${EOS_DIR}/plots/${RATETYPE}"
    mkdir -p ${PLOTS_DIR}

    execute "cp ./plots/*.pdf ${PLOTS_DIR}"
    execute "cp rates*.root ${PLOTS_DIR}"

    PUBLIC_SUBDIR="${PUBLIC_DIR}/plots/${RATETYPE}"
    mkdir -p ${PUBLIC_SUBDIR}
    execute "cp ./plots/*.pdf ${PUBLIC_SUBDIR}"

    cd ..
done

execute "cd ${CMSSW_BASE}/src/ConditionsValidation/"
execute "git pull origin master"

# Needed specifically for the environment for making the summary slides...
export BASE_PATH=${CMSSW_BASE}/src/ConditionsValidation
export OUTDIR=${EOS_DIR}
export SCRATCH_DIR=${WDIR}
export CMSSW_L1=${CMSSW_BASE}


# This code magically sends the overview.pdf to OUTDIR which is EOS_DIR
execute "python3 -m PostAnalysis configuration/post_conf.py"

execute "cp ${EOS_DIR}/overview.pdf ${PUBLIC_DIR}"

ecalautomation.py ${TASK} jobctrl --id ${JOB_ID} --done
