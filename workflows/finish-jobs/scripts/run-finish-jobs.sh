shift
shift
shift

CAMPAIGN=$1
shift

DBNAME=$1
shift

RUN=$1
shift

FILL=$1
shift

TASKNAME=$1
shift

JOBIDS=("$@")

for JOBID in "${JOBIDS[@]}"
do
    ecalautomation.py -w $TASKNAME -c $CAMPAIGN -t fill:$FILL,run_number:$RUN --db $DBNAME jobctrl --id $JOBID --done
done
