from ecalautoctrl import process_by_fill
from hcal_condor import HCALCondorHandler
from hcal_runtools import allowGroupOverride, maybeInt

import sys


@allowGroupOverride
@process_by_fill(fill_complete=False)
class FinishJobsHandler(HCALCondorHandler):
    def __init__(self, task_name, script_path, **kwargs):
        super().__init__(task_name, script_path, **kwargs)

        self.augmentParser(self.submit_parser)
        self.submit_parser.add_argument("--the-fill",  type = maybeInt, default = None)
        self.submit_parser.add_argument("--task-name", type = str,      default = None)
        self.submit_parser.add_argument("--job-range", type = str,      default = None)

        self.prevTaskNames = kwargs["deps_task"]

    def getWorkItems(self):
        self.reconfigMMlogger()

        run      = self.opts.the_run
        fill     = self.opts.the_fill
        taskName = self.opts.task_name
        jobList  = self.opts.job_range

        jobs = []
        # Consider if jobList is e.g. 3-6, which means all job ids between and including 3 and 6
        if "-" in jobList:
            temp = jobList.split("-")
            start = int(temp[0])
            end   = int(temp[-1])
            for job in range(start, end+1):
                jobs += str(job)

        # Consider if jobList is e.g. 0,2,7
        elif "," in jobList:
            jobs = jobList.split(",")

        # Otherwise, a single job id is passed
        else:
            jobs.append(jobList)
    
        args = [{"arguments": f"{self.campaign} {self.opts.dbname} {run} {fill} {taskName} {jobs}"}]

        return args


if __name__ == "__main__":

    handler = FinishJobsHandler("finish-jobs", "scripts/run-finish-jobs.sh", deps_task = None)
    ret_code = handler()
    sys.exit(ret_code)
