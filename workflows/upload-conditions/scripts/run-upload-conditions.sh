# First three arguments are always task name e.g. make-ntuples, job id related to condor job, and directory
# on EOS in the form /eos/cms/store/group/dpg_hcal/comm_hcal/AUTOMATION_TEST_AREA/${TASK}/${RUN}
TASK=${1}
JOB_ID=${2}
EOS_DIR=${3}
PUBLIC_DIR=${4}

RUN_NUMBER=${5}

NEW_CONDS_TAR=${6}

TRIGGERKEY_FILE=${7}
LUT_FILE=${8}
ZS_FILE=${9}
L1TRGOBJ_FILE=${10}

DO_DRY_RUN=${11}

INCLUDE_PEDESTALS=${12}
INCLUDE_GAINS=${13}

LATEST_PEDESTALS_DIR=${14}
LATEST_GAINS_DIR=${15}

PEDESTALS_TAG=${16}
PEDESTALWIDTHS_TAG=${17}
GAINS_TAG=${18}

print_args TASK JOB_ID EOS_DIR PUBLIC_DIR NEW_CONDS_TAR RUN_NUMBER TRIGGERKEY_FILE LUT_FILE ZS_FILE L1TRGOBJ_FILE DO_DRY_RUN INCLUDE_PEDESTALS INCLUDE_GAINS LATEST_PEDESTALS_DIR LATEST_GAINS_DIR PEDESTALS_TAG PEDESTALWIDTHS_TAG GAINS_TAG

YEAR=$(date +%Y)

ecalautomation.py ${TASK} jobctrl --id ${JOB_ID} --running

cd ${CMSSW_BASE}/src
cp ConditionsValidation/Tools/writetoSQL.csh .
chmod u+x writetoSQL.csh

# All to-be-uploaded conditions will be based on the txt in the tar-ed up conditions folder
# that was dumped and updated by the lut-production task
execute "cp ${NEW_CONDS_TAR} ."
execute "tar -xvzf $(basename ${NEW_CONDS_TAR})"

# Will send commands to be run and etc to receipt file for review
RECEIPT="upload_receipt.txt"

if [[ ${DO_DRY_RUN} -eq 0 ]]; then
    echo "Running the below commands !" >> ${RECEIPT}
else
    echo "Doing a dry run, below commands would be run" >> ${RECEIPT}
fi
echo -e "\n\n" >> ${RECEIPT}

function make_sidecar_file() {
    local CONDITION=$1
    local FILENAME=$2

    echo "${FILENAME}.txt" >> ${RECEIPT}

    echo '{' > ${FILENAME}.txt
    echo '    "destinationDatabase": "oracle://cms_orcon_prod/CMS_CONDITIONS", ' >> ${FILENAME}.txt
    echo '    "destinationTags": {' >> ${FILENAME}.txt
    echo '        "'${FILENAME}'": {}' >> ${FILENAME}.txt
    echo '    },' >> ${FILENAME}.txt
    echo '    "inputTag": "Tag",' >> ${FILENAME}.txt
    echo '    "since": '${RUN_NUMBER}',' >> ${FILENAME}.txt
    echo '    "userText": "'${YEAR}' new '${CONDITION}' data tag"' >> ${FILENAME}.txt
    echo '}' >> ${FILENAME}.txt

    echo "CondDB sidecar txt file for ${CONDITION}:" >> ${RECEIPT}
    cat ${FILENAME}.txt >> ${RECEIPT}
    echo -e "\n\n" >> ${RECEIPT}
}

function check_command() {
    local command=$1
    local source=$2
    local dest=$3

    execute "ls -l ${source}"

    if [[ ! -z "${dest}" ]]; then
        execute "ls -l $(dirname ${dest})"
    fi
}

function prepare_for_upload() {
    local CONDITION=$1
    local NICKNAME=$2
    local DB_FILENAME=$3
    local CACHE_DIR=$4
    local UPLOAD_CMD_EXPORT=$5
    local COPY_CMD_EXPORT=$6

    # Extract the pedestals txt from the untar'd conditions folder
    execute "cp conditions/${CONDITION}/${CONDITION}_Run*.txt ${CONDITION}.txt"

    # First pedestals DB and sidecar
    execute "./writetoSQL.csh ${YEAR} ${NICKNAME} ${CONDITION}.txt Tag ${RUN_NUMBER} ${DB_FILENAME}.db >> ${RECEIPT}"
    ls -l ${DB_FILENAME}.db >> ${RECEIPT}

    echo -e "\n\n" >> ${RECEIPT}

    make_sidecar_file ${NICKNAME} ${DB_FILENAME}

    # Commands for uploading and caching pedestals
    UPLOAD_CMD="uploadConditions.py ${DB_FILENAME}.db"
    COPY_CMD="cp ${CONDITION}.txt ${CACHE_DIR}/${CONDITION}.txt"

    # Check that the commands will work
    check_command ${UPLOAD_CMD}
    check_command ${COPY_CMD}

    echo "Commands to run:" >> ${RECEIPT}
    echo "${UPLOAD_CMD}" >> ${RECEIPT}
    echo "${COPY_CMD}" >> ${RECEIPT}
    echo -e "\n\n" >> ${RECEIPT}

    eval ${UPLOAD_CMD_EXPORT}="'${UPLOAD_CMD}'"
    eval ${COPY_CMD_EXPORT}="'${COPY_CMD}'"
}

# If requested to upload pedestal conditions, make DB files and sidecar files
if [[ ${INCLUDE_PEDESTALS} -eq 1 ]]; then

    # Commands for uploading and caching pedestals
    UPLOAD_PEDS_CMD=""
    COPY_PEDS_CMD=""

    prepare_for_upload EffectivePedestals Pedestals ${PEDESTALS_TAG} ${LATEST_PEDESTALS_DIR} UPLOAD_PEDS_CMD COPY_PEDS_CMD

    ###########################################################################################################
    
    # Store the commands to be evaluated for uploading and storing the latest pedestal widths
    UPLOAD_PEDWIDTHS_CMD=""
    COPY_PEDWIDTHS_CMD=""

    prepare_for_upload EffectivePedestalWidths PedestalWidths ${PEDESTALWIDTHS_TAG} ${LATEST_PEDESTALS_DIR} UPLOAD_PEDWIDTHS_CMD COPY_PEDWIDTHS_CMD
  
    UPDATE_PEDS_RUNNUM_CMD="echo ${RUN_NUMBER} > ${LATEST_PEDESTALS_DIR}/run_number.txt"
    echo "${UPDATE_PEDS_RUNNUM_CMD}" >> ${RECEIPT}
    echo -e "\n\n" >> ${RECEIPT}
fi

#########################################################################################

# If requested to upload gains conditions, make a DB and sidecar file
if [[ ${INCLUDE_GAINS} -eq 1 ]]; then

    # Commands for uploading and caching pedestals
    UPLOAD_GAINS_CMD=""
    COPY_GAINS_CMD=""

    prepare_for_upload Gains Gains ${GAINS_TAG} ${LATEST_GAINS_DIR} UPLOAD_GAINS_CMD COPY_GAINS_CMD

    UPDATE_GAINS_RUNNUM_CMD="echo ${RUN_NUMBER} > ${LATEST_GAINS_DIR}/run_number.txt"
    echo "${UPDATE_GAINS_RUNNUM_CMD}" >> ${RECEIPT}
    echo -e "\n\n" >> ${RECEIPT}
fi

#########################################################################################

# Make the sidecar file for uploading L1TriggerObjects to CondDB
execute "cp ${L1TRGOBJ_FILE} ."
L1TRGOBJ_FILE=$(basename ${L1TRGOBJ_FILE})

make_sidecar_file L1TriggerObjects ${L1TRGOBJ_FILE%.*}

# Prepare commands for uploading and copying files for xmldb uploader to see
UPLOAD_L1TRGOBJS_CMD="uploadConditions.py ${L1TRGOBJ_FILE}"
COPY_TK_XML_CMD="cp ${TRIGGERKEY_FILE} ${TRIGGERKEY_FILE/tmp\//}"
COPY_LUT_XML_CMD="cp ${LUT_FILE} ${LUT_FILE/tmp\//}"
COPY_ZS_XML_CMD="cp ${ZS_FILE} ${ZS_FILE/tmp\//}"

check_command ${UPLOAD_L1TRGOBJS_CMD}
check_command ${COPY_TK_XML_CMD}
check_command ${COPY_LUT_XML_CMD}
check_command ${COPY_ZS_XML_CMD}

echo "Commands to run:" >> ${RECEIPT}
echo "${UPLOAD_L1TRGOBJS_CMD}" >> ${RECEIPT}
echo "${COPY_TK_XML_CMD}" >> ${RECEIPT}
echo "${COPY_LUT_XML_CMD}" >> ${RECEIPT}
echo "${COPY_ZS_XML_CMD}" >> ${RECEIPT}

#########################################################################################

# With everything prepared check if doing dry run or not and execute commands accordingly
if [[ ${DO_DRY_RUN} -eq 0 ]]; then
    echo "This is not a dry run - running the upload and cp commands !"

    echo "Uploading L1TriggerObjects to CondDB"
    execute "${UPLOAD_L1TRGOBJS_CMD}"

    echo "Copying trigger key for getting up for XMLDB"
    execute "${COPY_TK_XML_CMD}"
    execute "${COPY_LUT_XML_CMD}"
    execute "${COPY_ZS_XML_CMD}"

    if [[ ${INCLUDE_PEDESTALS} -eq 1 ]]; then
        echo "Uploading pedestals to CondDB..."
        execute "${UPLOAD_PEDS_CMD}"

        echo "Uploading pedestal widths to CondDB..."
        execute "${UPLOAD_PEDWIDTHS_CMD}"

        echo "Copying pedestals to latest_pedestals area..."
        # Update the "latest pedestals" with the ones we just created
        execute "${COPY_PEDS_CMD}"
        execute "${COPY_PEDWIDTHS_CMD}"
        execute "${UPDATE_PEDS_RUNNUM_CMD}"
    fi

    if [[ ${INCLUDE_GAINS} -eq 1 ]]; then
        echo "Uploading gains to CondDB..."
        execute "${UPLOAD_GAINS_CMD}"

        echo "Copying gains to latest_gains area..."
        # Update the "latest gains" with the ones we just created
        execute "${COPY_GAINS_CMD}"
        execute "${UPDATE_GAINS_RUNNUM_CMD}"
    fi
fi

echo -e "\n\n" >> ${RECEIPT}
echo "DONE !" >> ${RECEIPT}

# Save the receipt in the upload-conditions/runNumber output folder
cp ${RECEIPT} ${EOS_DIR}
cp ${RECEIPT} ${PUBLIC_DIR}

ecalautomation.py ${TASK} jobctrl --done --id ${JOB_ID}
