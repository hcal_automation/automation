from ecalautoctrl import process_by_fill, JobCtrl
from hcal_condor import HCALCondorHandler
from hcal_runtools import allowGroupOverride, maybeBool

import os
import sys


@allowGroupOverride
class UploadCondsHandler(HCALCondorHandler):
    def __init__(self, task_name, script_path, **kwargs):
        super().__init__(task_name, script_path, **kwargs)

        self.augmentParser(self.submit_parser)

        self.submit_parser.add_argument("--dry-run", type = maybeBool, default = None)

        self.prevTaskNames = kwargs["deps_task"]

    # Check what the user intends to upload and compare against what was put into the L1TriggerObjs
    # There must be a one-to-one correspondance between which conditions are uploaded and what was put
    # in the L1TriggerObjects - any other way is simply incorrect and not allowed
    def checkUploadConsistency(self, condition, lutTag, lutGenJob, consistent):

        # Determine whether the user requests that the particular condition to be uploaded
        uploadCondition = False
        if getattr(self.opts, f"include_{condition}"):
            uploadCondition = True

        # Go to the previous LUT generation task and see what was the input for the particular condition
        conditionInput = None
        conditionTag = f"{condition}_input"
        includedInLUT = False
        if conditionTag in lutGenJob:
            conditionInput = lutGenJob[conditionTag]
            # The only way that condition is counted as included in LUT is if the input is a legit file
            if os.path.isfile(conditionInput):
                includedInLUT = True
       
        # "Call the user's bluff" about what they want to upload vs what is in the L1TriggerObjects/LUT
        templateStatement = f"A user requested to__USERREQ__upload new __CONDITION__ conditions based on {self.runAndFill}; however, the latest trigger key with tag {lutTag} suggests that new __CONDITION__ are__INCONDS__included in the LUT and L1TriggerObjects. Either choose to __UPLOAD__ __CONDITION__ via the appropriate checkboxes on the hcal-UploadConditions GUI or rerun the hcal-LUT_TK_L1TrgObjs pipeline and __GENERATE__ __CONDITION__ and then try running hcal-UploadConditions again."
   
        finalStatement = None
        if includedInLUT and not uploadCondition:
            finalStatement = templateStatement.replace("__USERREQ__", " NOT ").replace("__INCONDS__", " ") \
                                              .replace("__UPLOAD__", "include").replace("__GENERATE__", "exclude") \
                                              .replace("__CONDITION__", condition) 

        elif not includedInLUT and uploadCondition:
            finalStatement = templateStatement.replace("__USERREQ__", " ").replace("__INCONDS__", " NOT ") \
                                              .replace("__UPLOAD__", "exclude").replace("__GENERATE__", "include") \
                                              .replace("__CONDITION__", condition) 

        if finalStatement:
            self.log.error(f"{finalStatement}")

        return consistent and finalStatement == None, includedInLUT

    def getWorkItems(self, runObj):
        self.reconfigMMlogger()

        # Stop with obviously useless situation where user requests nothing to be uploaded
        if not self.opts.include_pedestals and not self.opts.include_gains:
            self.log.error(f"When running the hcal-UploadConditions pipeline at least one of \"IncludePedestals\" and/or \"IncludeGains\" checkboxes must be selected on the GUI. Review desired conditions to upload, make the appropriate selection(s), and run the hcal-UploadConditions pipeline again.")
            return None, self.incompleteTaskStatus

        runNumber  = runObj["run_number"]
        fillNumber = runObj["fill"]

        # Get the LUT generation task to get the LUT/TK/L1TrgObjs files and conditions tar and LUT tag
        lutGenTask = JobCtrl(
            task     = self.prevTaskNames[0],
            campaign = self.opts.campaign[0],
            tags     = {"run_number": runNumber, "fill": fillNumber},
            dbname   = self.opts.dbname,
        )
        # The zeroth of two lut-production jobs is for the new condtions
        lutGenJob          = lutGenTask.getJob(0, last = True)[0]
        triggerKeyFile     = lutGenJob["trigger_key"]
        lutFile            = lutGenJob["lut"]
        newLutTag          = lutGenJob["lut_tag"]
        newConditionsTar   = lutGenJob["conditions_tar"]
        newL1TrgObjsDBfile = lutGenJob["l1_trg_objs_db"]
        pedestalsTag       = lutGenJob["pedestals_tag"]
        pedestalWidthsTag  = lutGenJob["pedestalwidths_tag"]
        gainsTag           = lutGenJob["gains_tag"]

        # Get the ZS file from the pedestals-production task
        pedProdTask = JobCtrl(
            task     = self.prevTaskNames[1],
            campaign = self.opts.campaign[0],
            tags     = {"run_number": runNumber, "fill": fillNumber},
            dbname   = self.opts.dbname,
        )
        pedProdJob = pedProdTask.getJob(0, last = True)[0]
        zsFile     = pedProdJob["zs_file"]

        uploadConsistent = True
        uploadConsistent, newPedestalsInLUT = self.checkUploadConsistency("pedestals", newLutTag, lutGenJob, uploadConsistent)
        uploadConsistent, newGainsInLUT     = self.checkUploadConsistency("gains",     newLutTag, lutGenJob, uploadConsistent)

        # If upload request and actual new conditions in LUT/L1TriggerObjects not consistent
        # Return here with an appropriate task status
        if not uploadConsistent:
            return None, self.incompleteTaskStatus

        dryRunText = ""
        if self.opts.dry_run:
            dryRunText = "This is a dry run... "

        self.log.info(f"{dryRunText}Preparing for uploading new conditions based on run {self.runAndFill}. To use files in \"{newConditionsTar}\" with tag \"{newLutTag}\" and DB file \"{newL1TrgObjsDBfile}\". Upon condor job completion, see the \"conditions_upload_receipts\" public output folder.")

        # If making it here, the consistency check passed - the user requests the new conditions in the LUT
        # to be uploaded. Thus, boolean for if a new condition is in the LUT is equivalent to the user request
        # to upload the new condition
        args = [{"arguments": f"{runNumber} {newConditionsTar} {triggerKeyFile} {lutFile} {zsFile} {newL1TrgObjsDBfile} {int(self.opts.dry_run)} {int(newPedestalsInLUT)} {int(newGainsInLUT)} {self.opts.prev_pedestals_dir} {self.opts.prev_gains_dir} {pedestalsTag} {pedestalWidthsTag} {gainsTag}"}]

        return args, None



if __name__ == "__main__":

    handler = UploadCondsHandler("upload-conditions", "scripts/run-upload-conditions.sh", deps_task = ["lut-production", "pedestals-production"])
    ret_code = handler()
    sys.exit(ret_code)
