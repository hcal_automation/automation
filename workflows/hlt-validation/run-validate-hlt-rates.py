from ecalautoctrl import process_by_fill
from hcal_condor import HCALCondorHandler
from hcal_runtools import allowGroupOverride

import sys


@allowGroupOverride
@process_by_fill(fill_complete=False)
class HLTRatesValidationHandler(HCALCondorHandler):
    def __init__(self, task_name, script_path, **kwargs):
        super().__init__(task_name, script_path, **kwargs)

        self.augmentParser(self.submit_parser)

        self.prevTaskNames = kwargs["deps_task"]


    def getWorkItems(self, runObj):
        self.reconfigMMlogger()

        runNumber = runObj["run_number"]

        hlt_logs = f"{self.opts.eosdir}/{self.prevTaskNames[0]}/{runNumber}"

        args =  [{"arguments": hlt_logs}]

        return args, None



if __name__ == "__main__":

    handler = HLTRatesValidationHandler("hlt-rates-validation", "scripts/run-validate-hlt-rates.sh", deps_task = ["hlt-rates-production"])
    ret_code = handler()
    sys.exit(ret_code)
