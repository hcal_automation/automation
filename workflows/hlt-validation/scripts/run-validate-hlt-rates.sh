#!/usr/bin/env bash

# First three arguments are always task name e.g. make-ntuples, job id related to condor job, and directory
# on EOS in the form /eos/cms/store/group/dpg_hcal/comm_hcal/AUTOMATION_TEST_AREA/${TASK}/${RUN}
TASK=${1}
JOB_ID=${2}
EOS_DIR=${3}
PUBLIC_DIR=${4}

# Contains txt output from HLT menu cmsRun for reference conds and for new conds
LOG_PATH=${5}

print_args TASK JOB_ID EOS_DIR PUBLIC_DIR LOG_PATH

ecalautomation.py ${TASK} jobctrl --id ${JOB_ID} --running

execute "cp ${LOG_PATH}/*.txt ."

declare -A ref_passing_events
declare -A ref_total_events
declare -A new_passing_events
declare -A new_total_events

# Loop over all job txt files containing HLT rate pass/total counts for storing
function extract_counts() {
    declare -n passing_events=$2
    declare -n total_events=$3

    for file in "./$1"*.txt; do
        if [[ -f "${file}" ]]; then
            echo ${file}
            cat ${file}
            echo -e "\n\n"

            line=$(cat "${file}")
            matches=$(echo "${line}" | grep -o -E "HLT_[^:]+:[0-9]+ [^:]+:[0-9]+")
            index=1
            while IFS= read -r match; do
                var_name=$(echo "${match}" | awk -F '[: ]' '{print $1}')
                number_passing=$(echo "${match}" | awk -F '[: ]' '{print $2}')
                number_total=$(echo "${match}" | awk -F '[: ]' '{print $4}')
                num_fields=$(echo "${match}" | awk -F '[: ]' '{print NF}')
    
                if (( num_fields < 4 )); then
                    continue
                fi
    
                if [[ -n "${ref_passing_events[${var_name}]}" ]]; then
                  passing_events["${var_name}"]=$((passing_events["${var_name}"] + number_passing))
                else
                  passing_events["${var_name}"]=${number_passing}
                fi
    
                if [[ -n "${ref_total_events[${var_name}]}" ]]; then
                  total_events["${var_name}"]=$((total_events["${var_name}"] + number_total))
                else
                  total_events["${var_name}"]=${number_total}
                fi
    
            done <<< "${matches}"
        fi
    done
}

execute "extract_counts Ref ref_passing_events ref_total_events"
execute "extract_counts New new_passing_events new_total_events"

function compute_rates() {
    declare -n output_rates=$3
    pass_events=$1
    total_events=$2
    fail_events=$(echo "${total_events} - ${pass_events}" | bc)
    pass_over_total=$(echo "scale=16; ${pass_events} / ${total_events}" | bc)
    rate=$(echo "scale=16; ${pass_events} * 88800 / ${total_events}" | bc)
    fail_over_pass=$(echo "scale=16; ${fail_events} / ${pass_events}" | bc)
    unc_term_one=$(echo "scale=24; (${pass_over_total} * ${pass_over_total}) / (${total_events} * ${total_events})" | bc)
    unc_term_two=$(echo "scale=16; ${fail_over_pass} * ${fail_over_pass} * ${pass_events}" | bc)
    unc_term_thr=$(echo "scale=16; ${fail_events}" | bc)
    rate_unc=$(echo "scale=16; 88800 * sqrt( ${unc_term_one} * (${unc_term_two} + ${unc_term_thr}) )" | bc)
    rate_unc_frac2=$(echo "scale=16; (${rate_unc} / ${rate}) * (${rate_unc} / ${rate})" | bc)

    output_rates[rate]=${rate}
    output_rates[rate_unc]=${rate_unc}
    output_rates[rate_unc_frac2]=${rate_unc_frac2}
    output_rates[pass_events]=${pass_events}
    output_rates[total_events]=${total_events}
}

HLT_RATE_CHANGES_TXT="HLT_Rate_Log.txt"

# Loop over the different HLT paths and compute rate diffs and send to output txt file
for var_name in "${!new_total_events[@]}"; do

  declare -A ref_output
  declare -A new_output

  execute "compute_rates ${ref_passing_events[${var_name}]} ${ref_total_events[${var_name}]} ref_output"
  execute "compute_rates ${new_passing_events[${var_name}]} ${new_total_events[${var_name}]} new_output"

  rate_ratio_2=$(echo "scale=16; (${ref_output['rate']} / ${new_output['rate']}) * (${ref_output['rate']} / ${new_output['rate']})" | bc)
  rate_ratio_unc=$(echo "scale=16; 100 * sqrt( ${rate_ratio_2} * (${ref_output['rate_unc_frac2']} + ${new_output['rate_unc_frac2']}) )" | bc)
  rate_diff=$(echo "scale=16; (${new_output['rate']} - ${ref_output['rate']}) * 100 / ${ref_output['rate']}" | bc)

  rate_diff_abs=$(echo "${rate_diff}" | bc <<< "if (${rate_diff} < 0) -(${rate_diff}) else ${rate_diff}")

  printf "${var_name}\n" >> ${HLT_RATE_CHANGES_TXT}
  printf "    Reference: %4s / %6s = %5.1f +/- %4.1f\n" "${ref_output['pass_events']}" "${ref_output['total_events']}" "${ref_output['rate']}" "${ref_output['rate_unc']}" >> ${HLT_RATE_CHANGES_TXT}
  printf "    New:    %4s / %6s = %5.1f +/- %4.1f\n" "${new_output['pass_events']}" "${new_output['total_events']}" "${new_output['rate']}" "${new_output['rate_unc']}" >> ${HLT_RATE_CHANGES_TXT}
  printf "    Rate Diff:               = %5.1f +/- %4.1f\n" "${rate_diff}" "${rate_ratio_unc}" >> ${HLT_RATE_CHANGES_TXT}

  threshold=5
  if (( $(echo "${rate_diff_abs} > ${threshold}" | bc -l) )); then
      if (( $(echo "${rate_diff_abs} < ${rate_ratio_unc}" | bc -l) )); then
        echo "    ==> Rate change > |5%|, but consistent (within uncertainty) of no change" >> ${HLT_RATE_CHANGES_TXT}
      else
        echo "    ==> Rate change > |5%|, and significant" >> ${HLT_RATE_CHANGES_TXT}
      fi
  fi
  echo -e "\n" >> ${HLT_RATE_CHANGES_TXT}
done

execute "cp ${HLT_RATE_CHANGES_TXT} ${EOS_DIR}"

execute "cp ${HLT_RATE_CHANGES_TXT} ${PUBLIC_DIR}"
 
ecalautomation.py ${TASK} jobctrl --id ${JOB_ID} --done --fields "rate_changes_txt:${EOS_DIR}/${HLT_RATE_CHANGES_TXT}"
