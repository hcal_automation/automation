#!/usr/bin/env bash

# First three arguments are always task name e.g. make-ntuples, job id related to condor job, and directory
# on EOS in the form /eos/cms/store/group/dpg_hcal/comm_hcal/AUTOMATION_TEST_AREA/${TASK}/${RUN_NUMBER}
TASK=${1}
JOB_ID=${2}
EOS_DIR=${3}

INPUT_FILE=${4}
GLOBAL_TAG=${5}
RUN_NUMBER=${6}
NEW_CONDITIONS_TAR=${7}
REF_CONDITIONS_TAR=${8}
PEDESTALS_TAR=${9}
GAINS_TAR=${10}

print_args TASK JOB_ID EOS_DIR INPUT_FILE GLOBAL_TAG RUN_NUMBER NEW_CONDITIONS_TAR REF_CONDITIONS_TAR PEDESTALS_TAR GAINS_TAR

ecalautomation.py ${TASK} jobctrl --id ${JOB_ID} --running

cd ${CMSSW_BASE}/src/

# Latest txt files to come from the conditions tar, which are reformatted and contain all channels
execute "cp ${NEW_CONDITIONS_TAR} ."
execute "tar -xvzf $(basename ${NEW_CONDITIONS_TAR}) && mv conditions conditionsNew"

execute "cp ${REF_CONDITIONS_TAR} ."
execute "tar -xvzf $(basename ${REF_CONDITIONS_TAR}) && mv conditions conditionsRef"

execute "hltGetConfiguration run:${RUN_NUMBER} --globaltag ${GLOBAL_TAG} --data --unprescale --output minimal --max-events -1 --input "${INPUT_FILE}" > hltRef.py"

echo "process.schedule.remove(process.DQMHistograms)" >> hltRef.py
echo "process.options.numberOfThreads = 16" >> hltRef.py

# cmsRun configs for reference HLT rates and new (target) hlt rates are identical up to this point
cp hltRef.py hltNew.py

function load_conditions() {
    # $1 = reference or new conditions
    # $2 = conditions object e.g. EffectivePedestals

    if ! grep -q "HcalTextCalibrations" hlt$1.py; then
        echo "process.hltOutputMinimal.fileName = 'output$1.root'" >> hlt$1.py
        echo "process.es_prefer = cms.ESPrefer('HcalTextCalibrations','es_ascii')"  >> hlt$1.py
        echo "process.es_ascii  = cms.ESSource('HcalTextCalibrations'," >> hlt$1.py
        echo "  input = cms.VPSet("  >> hlt$1.py
    fi

    echo "    cms.PSet(" >> hlt$1.py
    echo "     object = cms.string('$2')," >> hlt$1.py
    echo "     file   = cms.FileInPath('$2$1.txt')" >> hlt$1.py
    echo "    )," >> hlt$1.py
}

CONDITIONS=("EffectivePedestals" "EffectivePedestalWidths" "Gains")
NEW_OR_REF=("Ref" "New")

for TYPE in "${NEW_OR_REF[@]}"; do
    for CONDITION in "${CONDITIONS[@]}"; do
        # Based on if the pedestals tar is legitimate i.e. not "None" load into cmsRun config
        if [[ ("${CONDITION}" == *"Pedestal"* && -f ${PEDESTALS_TAR}) ||
              ("${CONDITION}" == *"Gains"* && -f ${GAINS_TAR}) ]]; then
        
            execute "cp conditions${TYPE}/${CONDITION}/${CONDITION}_Run*.txt ${CONDITION}${TYPE}.txt"
            load_conditions ${TYPE} ${CONDITION}
        fi
    done

    echo "  )" >> hlt${TYPE}.py
    echo ")" >> hlt${TYPE}.py
done

echo -e "\n\n"

execute "cmsRun hltRef.py >& logRef &"
sleep 5
execute "cmsRun hltNew.py >& logNew &"
wait

for TYPE in "${NEW_OR_REF[@]}"; do
    echo "Checking cmsRun log ${TYPE} files"
    cat log${TYPE}
    echo -e "\n\n"
done

execute "hltDiff -o outputRef.root -n outputNew.root"

pathToMonitor=("HLT_PFMETNoMu120_PFMHTNoMu120_IDTight" "HLT_PFMET250_NotCleaned" "HLT_PFHT250_QuadPFJet25_PNet2BTagMean0p55" "HLT_PFHT1050" "HLT_AK8PFJet380_MassSD30" "HLT_PFJet500" "HLT_PFMET120_PFMHT120_IDTight" "HLT_CaloJet500_NoJetID" "AlCa_PFJet40" "HLT_DoubleMediumChargedIsoDisplacedPFTauHPS32_Trk1_eta2p1" "HLT_DoubleMediumDeepTauPFTauHPS35_L2NN_eta2p1" "HLT_DoublePNetTauhPFJet30_Medium_L2NN_eta2p3" "HLT_Ele30_WPTight_Gsf" "HLT_Diphoton30_22_R9Id_OR_IsoCaloId_AND_HE_R9Id_Mass90" "HLT_HT360_DisplacedDijet45_Inclusive1PtrkShortSig5" "HLT_IsoMu24" "HLT_Mu50" "HLT_PFHT400_FivePFJet_120_120_60_30_30_PNet2BTag_5p6" "HLT_HT430_DisplacedDijet40_DisplacedTrack" "HLT_HT650_DisplacedDijet60_Inclusive" "HLT_HT430_DelayedJet40_DoubleDelay0p5nsTrackless")

for TYPE in "${NEW_OR_REF[@]}"; do
    INFO=""
    for TRG in ${pathToMonitor[*]}; do
        echo "Checking log${TYPE}"
        cat log${TYPE} | grep ${TRG}_v | grep 'TrigReport' 
        PASS=`cat log${TYPE} | grep ${TRG}_v | grep 'TrigReport' | grep -v '\-----' |  awk '{print $5}'`
        EVENTS=`cat log${TYPE} | grep ${TRG}_v | grep 'TrigReport' | grep -v '\-----' |  awk '{print $4}'`
        INFO=${INFO}" ${TRG}:${PASS}:${EVENTS}"
    done
    
    echo "${TYPE} events statistics:"
    echo ${INFO}
    echo ${INFO} >> ${TYPE}_events_${JOB_ID}.txt
    cp ${TYPE}_events_${JOB_ID}.txt ${EOS_DIR}

done

ecalautomation.py ${TASK} jobctrl --id ${JOB_ID} --done
