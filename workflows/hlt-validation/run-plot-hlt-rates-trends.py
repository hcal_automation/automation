from hcal_condor import HCALCondorHandler
from hcal_runtools import allowGroupOverride
from ecalautoctrl import process_by_fill

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat("")
ROOT.gStyle.SetPaintTextFormat("3.2f")
ROOT.gStyle.SetFrameLineWidth(2)
ROOT.gStyle.SetEndErrorSize(0)
ROOT.TGaxis.SetMaxDigits(3)
ROOT.TH1.SetDefaultSumw2()
ROOT.TH2.SetDefaultSumw2()

import os
import sys
import glob

@allowGroupOverride
@process_by_fill(fill_complete=False)
class HLTRatesChangeTrendsHandler(HCALCondorHandler):
    def __init__(self, task_name, script_path, **kwargs):
        super().__init__(task_name, script_path, **kwargs)

        self.augmentParser(self.submit_parser)

        self.prevTaskNames = kwargs["deps_task"]


    def extractRate(self, line):
    
        counts  = line.split(": ")[-1].split(", ")
        passing = float(counts[-1])
        total   = float(counts[0])
        failing = total - passing
        rate    = passing / total
    
        rateUnc = ((rate**2.0 / total**2.0) * ( (failing / passing)**2.0 * passing + failing ))**0.5
        rateReal = rate * 88800.0
        rateUncReal = rateUnc * 88800.0
    
        return rateReal, rateUncReal
    
    
    def getWorkItems(self, runObj):
        self.reconfigMMlogger()

        hltEosDir = f"{self.opts.eosdir}/{self.publicSubDir}"
    
        hltTrendsDir = hltEosDir + "/trendPlots"
        if not os.path.isdir(hltTrendsDir):
            os.mkdir(hltTrendsDir)

        runFolders = glob.glob(hltEosDir + "/*")
        runs = {}

        for runFolder in runFolders:
        
            # Skip paths to literal files, only want folders
            theRun = runFolder.split("/")[-1].rstrip("\n")

            # Cheeky way to only consider folders that are the run number themselves
            try:
                integerRun = int(theRun)
            except:
                continue

            runs[int(theRun)] = {}
        
            hltRateLog = runFolder + "/HLT_Rate_Log.txt"
        
            f = open(hltRateLog, "r")
            lines = f.readlines()
            f.close()
        
            hltPath = None
            paths = {}
        
            legacyFormatting = "rate diff" in lines[0] or "LARGE" in lines[0]
            superLegacyFormatting = False
            prehistoricFormatting = False
            if legacyFormatting:
                superLegacyFormatting = "HLT" in lines[1]
                prehistoricFormatting = "LARGE" in lines[0]
        
            for iLine in range(0, len(lines)):
        
                line = lines[iLine].rstrip("\n")
        
                if "HLT" not in line:
                    continue
        
                hltPath = None
                if prehistoricFormatting:
                    hltPath = line.split(" ")[2].rstrip(":")
                else:
                    hltPath = line.split(" ")[0]
        
                if legacyFormatting:
                    
                    refRateReal = 0.0; refRateUncReal = 0.0
                    newRateReal = 0.0; newRateUncReal = 0.0
                    if prehistoricFormatting:
                        chunks = line.split(": ")[-1].rstrip("\n").split(" ")
                        rateDiff = float(chunks[0])
        
                        paths[hltPath] = [rateDiff, 0.0]
        
                    else:
                        if superLegacyFormatting:
                            chunk = line.split(": ")[-1]
                            refRateReal = float(chunk.split(",")[1].strip(" "))
                            newRateReal = float(chunk.split(",")[2].strip(" "))
        
                            refRateUncReal = 0.0
                            newRateUncReal = 0.0
                        else:
                            if iLine > len(lines)-2:
                                continue
        
                            refLine = lines[iLine+1].rstrip("\n")
                            newLine = lines[iLine+2].rstrip("\n")
        
                            refRateReal, refRateUncReal = self.extractRate(refLine)
                            newRateReal, newRateUncReal = self.extractRate(newLine)
        
                        rateDiff    = (newRateReal / refRateReal - 1.0) * 100.0
                        rateDiffUnc = ((newRateReal / refRateReal)**2.0 * ((newRateUncReal / newRateReal)**2.0 + (refRateUncReal / refRateReal)**2.0))**0.5 * 100.0
        
                        paths[hltPath] = [rateDiff, rateDiffUnc]
        
                else:
                    rateDiffLine = lines[iLine+3]
        
                    chunks = rateDiffLine.split("= ")
                    diffNumbers = chunks[-1].split(" +/- ")
        
                    rateDiff    = float(diffNumbers[0].strip(" "))
                    rateDiffUnc = float(diffNumbers[1].strip(" "))
        
                    paths[hltPath] = [rateDiff, rateDiffUnc]
        
            runs[int(theRun)] = paths
        
        sortedRuns = sorted(runs.keys())
        runRatesPerPath = {}
        for run in sortedRuns:
            paths = runs[run]
        
            for path, rateDiffObj in paths.items():
        
                if path not in runRatesPerPath:
                    runRatesPerPath[path] = {}
        
                runRatesPerPath[path][run] = rateDiffObj
                
        
        for path, runDict in runRatesPerPath.items():
        
            runs = []
            rateDiffs = []
            rateDiffUncs = []
        
            for run in sorted(runDict.keys()):
        
                rateDiffObj = runDict[run]
        
                runs.append(run)
                rateDiffs.append(rateDiffObj[0])
                rateDiffUncs.append(rateDiffObj[1])
        
        
            canvas = ROOT.TCanvas(path, path, 1600, 900)
        
            ROOT.gPad.SetTopMargin(0.02)
            ROOT.gPad.SetBottomMargin(0.12)
            ROOT.gPad.SetRightMargin(0.05)
        
            histo = ROOT.TH1F("h_" + path, ";Run;#Delta Rate (% points)", len(runs), -0.5, len(runs)+0.5)
            histo.SetLineColor(ROOT.kBlue)
            histo.SetMarkerColor(ROOT.kBlue)
            histo.SetLineWidth(2)
            histo.SetMarkerSize(2)
            histo.SetMarkerStyle(8)
        
            histo.GetYaxis().SetLabelSize(0.05)
            histo.GetYaxis().SetTitleSize(0.07)
            histo.GetYaxis().SetTitleOffset(0.6)
        
            histo.GetXaxis().SetLabelSize(0.05)
            histo.GetXaxis().SetTitleSize(0.07)
            histo.GetXaxis().SetTitleOffset(0.8)
        
            for iRun in range(0, len(runs)):
                histo.SetBinContent(iRun+1, rateDiffs[iRun])
                histo.SetBinError(iRun+1, rateDiffUncs[iRun])
                histo.GetXaxis().SetBinLabel(iRun+1, str(runs[iRun]))
        
            canvas.cd()
            histo.Draw("EP")
        
            line = ROOT.TLine(-0.5, 0.0, len(runs)+0.5, 0.0)
            line.SetLineWidth(2)
            line.SetLineStyle(2)
            line.SetLineColor(ROOT.kBlack)
            line.Draw("SAME")
        
            histo.Draw("EP SAME")
        
            canvas.SaveAs(path + ".pdf")
        
        os.system(f"mv *.pdf {hltTrendsDir}")
    
        return None, None

if __name__ == "__main__":

    handler = HLTRatesChangeTrendsHandler("hlt-rates-trends", "", deps_task = ["hlt-rates-changes"])
    ret_code = handler()
    sys.exit(ret_code)
