from hcal_condor import HCALCondorHandler
from hcal_runtools import allowGroupOverride
from ecalautoctrl import process_by_fill, JobCtrl

import os
import sys


@allowGroupOverride
@process_by_fill(fill_complete=False)
class HLTRatesProductionHandler(HCALCondorHandler):
    def __init__(self, task_name, script_path, **kwargs):
        super().__init__(task_name, script_path, **kwargs)

        self.augmentParser(self.submit_parser)

        self.prevTaskNames = kwargs["deps_task"]

        self.dataSetName = None
        self.runForFiles = None

    def getWorkItems(self, runObj):
        self.reconfigMMlogger()

        runNumber  = runObj["run_number"]
        fillNumber = runObj["fill"]

        # Comes from the Jenkins GUI
        self.dataSetName = self.opts.dataset
        self.runForFiles = self.opts.val_run

        lutGenTask = JobCtrl(
            task     = self.prevTaskNames[0],
            campaign = self.opts.campaign[0],
            tags     = {"run_number": runNumber, "fill": fillNumber},
            dbname   = self.opts.dbname,
        )
        newLutGenJob = lutGenTask.getJob(0, last = True)[0]
        refLutGenJob = lutGenTask.getJob(1, last = True)[0]

        pedestalsTar     = newLutGenJob["pedestals_input"]
        gainsTar         = newLutGenJob["gains_input"]
        newConditionsTar = newLutGenJob["conditions_tar"]

        refConditionsTar = refLutGenJob["conditions_tar"]

        pedestalsAndOrGains = []
        if os.path.isfile(pedestalsTar):
            pedestalsAndOrGains.append("pedestals")
        if os.path.isfile(gainsTar):
            pedestalsAndOrGains.append("gains")

        self.log.info(f"Running HLT rates for {self.runAndFill} with new {' and '.join(pedestalsAndOrGains)}.")
        
        rawFiles = self.getFilesDBS()
        if not rawFiles:
            self.log.error(f"Could not retrieve any valid RAW files from data set \"{self.dataSetName}\" for reference run {self.runForFiles}. Review the requested data set name and run the workflow again...")
            return None, self.reprocessTaskStatus

        argsStr = f"{self.opts.global_tag} {self.opts.val_run} {newConditionsTar} {refConditionsTar} {pedestalsTar} {gainsTar}"
        args = [{"arguments": f"{file} {argsStr}"} for iFile, file in enumerate(rawFiles) if iFile < self.opts.max_input_files]

        return args, None



if __name__ == "__main__":

    handler = HLTRatesProductionHandler("hlt-rates-production", "scripts/run-produce-hlt-rates.sh", deps_task = ["lut-production"])
    ret_code = handler()
    sys.exit(ret_code)
