from ecalautoctrl import process_by_fill, JobCtrl
from hcal_condor import HCALCondorHandler
from hcal_runtools import allowGroupOverride

import sys


@allowGroupOverride
@process_by_fill(fill_complete=False)
class LUTValHandler(HCALCondorHandler):
    def __init__(self, task_name, script_path, **kwargs):
        super().__init__(task_name, script_path, **kwargs)

        self.augmentParser(self.submit_parser)

        self.prevTaskNames = kwargs["deps_task"]

    def getWorkItems(self, runObj):
        self.reconfigMMlogger()

        runNumber  = runObj["run_number"]
        fillNumber = runObj["fill"]

        # Use the lut-production task to get the new conditions and ref conditions jobs
        # and all of their relevant output
        prevTask = JobCtrl(
            task     = self.prevTaskNames[0],
            campaign = self.opts.campaign[0],
            tags     = {"run_number": runNumber, "fill": fillNumber},
            dbname   = self.opts.dbname,
        )

        new_lut_job = prevTask.getJob(0, last = True)[0]
        ref_lut_job = prevTask.getJob(1, last = True)[0]

        new_lut_tag = new_lut_job["lut_tag"]
        new_conds   = new_lut_job["conditions_tar"]
        config_card = new_lut_job["config_card"]
        trigger_key = new_lut_job["trigger_key"]
        lut         = new_lut_job["lut"]
        l1_trgobjs  = new_lut_job["l1_trg_objs_db"]

        ref_lut_tag = ref_lut_job["lut_tag"]
        ref_conds   = ref_lut_job["conditions_tar"]

        args = [{"arguments": f"{new_lut_tag} {ref_lut_tag} {new_conds} {ref_conds} {config_card} {lut} {trigger_key} {l1_trgobjs}"}]

        return args, None



if __name__ == "__main__":

    handler = LUTValHandler("lut-validation", "scripts/run-validate-lut.sh", deps_task = ["lut-production"])
    ret_code = handler()
    sys.exit(ret_code)
