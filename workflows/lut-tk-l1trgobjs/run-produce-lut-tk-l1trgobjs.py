from ecalautoctrl import process_by_fill, JobCtrl
from hcal_condor import HCALCondorHandler
from hcal_runtools import allowGroupOverride

import os
import sys
import time
import calendar
import datetime


@allowGroupOverride
@process_by_fill(fill_complete=False)
class LUTProductionHandler(HCALCondorHandler):
    def __init__(self, task_name, script_path, **kwargs):
        super().__init__(task_name, script_path,**kwargs)

        self.augmentParser(self.submit_parser)
        self.submit_parser.add_argument("--lut-tag-aux", type = str, required = True)
        self.submit_parser.add_argument("--era",         type = str, default  = None)

        # The true check of previous (relevant) tasks is done via checking for those tasks outputs below
        self.prevTaskNames = kwargs["deps_task"]

    # Check what the user intends to put into the LUT and L1TriggerObjects
    # More specifically make sure pedestal and gain inputs are actually available
    def consistencyCheck(self, taskName, condition, run, fill, includeCondition, consistent):

        if includeCondition:
            # Access the conditions tar file via the job task
            jobTask = JobCtrl(
                task     = taskName,
                campaign = self.opts.campaign[0],
                tags     = {"run_number": run, "fill": fill},
                dbname   = self.opts.dbname,
            )
            theJobList = jobTask.getJob(0, last = True)

            # If there was no task before, the job list will be empty
            theJob    = None
            outputTar = None
            if theJobList != []:
                theJob    = theJobList[0]
                outputTar = theJob.get(condition, theJob.get("output", None))

            conditionInputExists = theJob and os.path.isfile(outputTar)

            if not conditionInputExists:
                # Cross-check that the desired condition to use actually exists
                self.log.error(f"A user requested to include new {condition} condition based on {self.runAndFill} for making a LUT/TK XML and L1TriggerObjects DB file; however, either the condition generation workflow has not been run or its output is missing. Verify that the desired condition is to be used and rerun the corresponding condition generation workflow.")
                return False, None

            return consistent & True, outputTar

        else:
            return consistent & True, None


    def getWorkItems(self, runObj):
        self.reconfigMMlogger()

        # Check corner case where user does not request any new conditions
        if not self.opts.include_pedestals and not self.opts.include_gains:
            self.log.error(f"A user requested to make a LUT/TK XML and L1TriggerObjects DB file based on {self.runAndFill}; however, both new pedestals and gains were excluded from consideration via the GUI. At least one of these conditions needs to be included.")
            return None, self.reprocessTaskStatus

        runNumber  = runObj["run_number"]
        fillNumber = runObj["fill"]

        # The reference LUT tag is simply a placeholder to be something different from the new LUT tag
        ref_lut_tag = "RefConditions"

        # The user may pass an addition string from the GUI to be appended to the new LUT tag
        appendToTag = ""
        if self.opts.lut_tag_aux != "":
            appendToTag = "_" + self.opts.lut_tag_aux 
            
        # Standard form of new LUT tag is Run3MmmYYYY_dd_appended e.g. Run3Dec2024_25_maskTT
        utcNow            = datetime.datetime.utcnow()
        yearAndDay        = utcNow.strftime('%Y_%d')
        monthThreeLetters = calendar.month_abbr[int(utcNow.strftime('%m'))]
        new_lut_tag       = f"Run3{monthThreeLetters}{yearAndDay}{appendToTag}"

        # Based on what the user requests to include in the LUT, TK, etc from the GUI,
        # check that the relevant condition tar file inputs are available
        consistent = True
        consistent, pedestalsTar = self.consistencyCheck("pedestals-production", "pedestals", runNumber, fillNumber, self.opts.include_pedestals, consistent)
        consistent, gainsTar     = self.consistencyCheck("gains-production",     "gains",     runNumber, fillNumber, self.opts.include_gains,     consistent)

        if not consistent:
            return None, self.incompleteTaskStatus

        argStr    = f"{self.opts.global_tag} {self.opts.era} __LUT_TAG__ {runNumber} {pedestalsTar} {gainsTar} {self.opts.prev_pedestals_dir} {self.opts.prev_gains_dir}"
        argStrNew = argStr.replace("__LUT_TAG__", new_lut_tag)
        argStrRef = argStr.replace("__LUT_TAG__", ref_lut_tag)

        return  [{"arguments" : f"{argStrNew} new"}, {"arguments" : f"{argStrRef} ref"}], None



if __name__ == "__main__":

    handler = LUTProductionHandler("lut-production", "scripts/run-produce-lut-tk-l1trgobjs.sh", deps_task = None)
    ret_code = handler()
    sys.exit(ret_code)
