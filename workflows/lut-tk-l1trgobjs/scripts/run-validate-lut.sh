#!/usr/bin/env bash

# First three arguments are always task name e.g. make-ntuples, job id related to condor job, and directory
# on EOS in the form /eos/cms/store/group/dpg_hcal/comm_hcal/AUTOMATION_TEST_AREA/${TASK}/$RUN
TASK=${1}
JOB_ID=${2}
EOS_DIR=${3}
PUBLIC_DIR=${4}

NEW_LUT_TAG=${5}
REF_LUT_TAG=${6}

NEW_CONDITIONS_ARCHIVE=${7}
REF_CONDITIONS_ARCHIVE=${8}

CARD_OUT=${9}
LUT_XML=${10}
TRIGGER_KEY=${11}
L1TRGOBJS_DB=${12}

print_args TASK JOB_ID EOS_DIR PUBLIC_DIR NEW_LUT_TAG REF_LUT_TAG NEW_CONDITIONS_ARCHIVE REF_CONDITIONS_ARCHIVE CARD_OUT LUT_XML TRIGGER_KEY L1TRGOBJS_DB

ecalautomation.py ${TASK} jobctrl --id $JOB_ID --running 

cd $CMSSW_BASE/src/CaloOnlineTools/HcalOnlineDb/test

function unpack_tar() {
    cp ${1} .
    tar -xzvf $(basename ${1})
    echo -e "\n\n"
}

execute "unpack_tar ${NEW_CONDITIONS_ARCHIVE}"
execute "unpack_tar ${REF_CONDITIONS_ARCHIVE}"

echo "Conditions folder contains:"
ls -lrt conditions
echo -e "\n\n"

echo "Performing diff of ref (${REF_LUT_TAG}) and new (${NEW_LUT_TAG}) LUT XML"
DIFF_OUTPUT_TXT="diff_${NEW_LUT_TAG}_vs_${REF_LUT_TAG}.txt"

./genLUT.sh diff card=${CARD_OUT} conditions/${NEW_LUT_TAG}/${NEW_LUT_TAG}.xml conditions/${REF_LUT_TAG}/${REF_LUT_TAG}.xml >> ${DIFF_OUTPUT_TXT}

# If the word "Comparison" in diff output, then diff ran to completion
execute "grep 'Comparison' ${DIFF_OUTPUT_TXT} >> /dev/null"

execute "cp ${DIFF_OUTPUT_TXT} ${EOS_DIR}"
execute "cp ${DIFF_OUTPUT_TXT} ${PUBLIC_DIR}"
execute "cp ${LUT_XML} ${PUBLIC_DIR}"
execute "cp ${TRIGGER_KEY} ${PUBLIC_DIR}"
execute "cp ${L1TRGOBJS_DB} ${PUBLIC_DIR}"
execute "cp ${CARD_OUT} ${PUBLIC_DIR}"

ecalautomation.py ${TASK} jobctrl --id $JOB_ID --done --fields "lut_diff:${EOS_DIR}/${DIFF_OUTPUT_TXT}"
