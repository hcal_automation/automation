#!/usr/bin/env bash

# First three arguments are always task name e.g. make-ntuples, job id related to condor job, and directory
# on EOS in the form /eos/cms/store/group/dpg_hcal/comm_hcal/AUTOMATION_TEST_AREA/$TASK/$RUN
TASK=${1}
JOB_ID=${2}
EOS_DIR=${3}

GLOBAL_TAG=${4}
ERA=${5}
LUT_TAG=${6}
NEW_RUN=${7}
PEDESTALS_TAR=${8}
GAINS_TAR=${9}
PREV_PEDESTALS_DIR=${10}
PREV_GAINS_DIR=${11}
NEW_OR_REF=${12}

# Use year when making L1TriggerObject DB file
THE_YEAR=$(date +%Y)

shopt -s extglob

print_args TASK JOB_ID EOS_DIR GLOBAL_TAG ERA LUT_TAG NEW_RUN PEDESTALS_TAR GAINS_TAR PREV_PEDESTALS_DIR PREV_GAINS_DIR NEW_OR_REF

ecalautomation.py $TASK jobctrl --running --id $JOB_ID --fields "lut_tag:${LUT_TAG}"

FUTURE_RUN=
if [[ ${NEW_OR_REF} == "new" ]]; then
    FUTURE_RUN=$((NEW_RUN + 1000))
else
    FUTURE_RUN=$((NEW_RUN + 999))
fi

# Tag names to be replaced in the TK XML file
EFFPEDS_TAG="HcalPedestals_effective"
EFFPEDWIDTHS_TAG="HcalPedestalWidths_effective"
GAINS_TAG="HcalGains_projection"
L1TRGOBJS_TAG="HcalL1TriggerObjects_${LUT_TAG}"

cd ${CMSSW_BASE}/src
eval `scramv1 runtime -sh`
cd ${CMSSW_BASE}/src/CaloOnlineTools/HcalOnlineDb/test
export CMSSW_SEARCH_PATH=${PWD}:${CMSSW_SEARCH_PATH}
echo -e "\n\n"

##########################################################################################################
# Some function definitions

function unpack_conditions() {
    local TAR_FILE=$1

    # Use the file exists check to conditionally unpack pedestals and/or gains
    if [[ -f ${TAR_FILE} ]]; then
        echo "Unpacking conditions in tar: ${TAR_FILE}"
        cp ${TAR_FILE} .
        tar -xvzf $(basename ${TAR_FILE})
        echo -e "\n\n"
    fi
}

function tar_and_cp() {
    local RETURN_VALUE=$1

    # Tar up everything in the conditions folder    
    CONDITIONS_TAR=${NEW_OR_REF}_conditions.tar.gz
    CONDITIONS_PATH=${EOS_DIR}/${CONDITIONS_TAR}

    execute "tar -czf ${CONDITIONS_TAR} conditions"
    execute "cp ${CONDITIONS_TAR} ${CONDITIONS_PATH}"

    eval "${RETURN_VALUE}=${CONDITIONS_PATH}"
}

function dump_conditions() {
    local CARD_CONFIG=$1

    > ${CARD_CONFIG}
    echo "# Pedestals input: ${PEDESTALS_TAR}" >> ${CARD_CONFIG}
    echo "# Gains input: ${GAINS_TAR}" >> ${CARD_CONFIG}
    echo GlobalTag=${GLOBAL_TAG} >> ${CARD_CONFIG}
    echo Era=${ERA} >> ${CARD_CONFIG}
    echo Tag=${LUT_TAG} >> ${CARD_CONFIG}
    echo Run=${FUTURE_RUN} >> ${CARD_CONFIG}
    echo description='"validation"' >> ${CARD_CONFIG}
    echo HOAsciiInput=HO_ped9_inputLUTcoderDec.txt >> ${CARD_CONFIG}

    # When making "new" conditions, also reference ref
    if [[ ${NEW_OR_REF} == "new" ]]; then
        echo applyO2O=true >> ${CARD_CONFIG}
    fi

    echo "Physics card is:"
    cat ${CARD_CONFIG}
    echo -e "\n\n"

    echo "Dumping conditions for new tag: ${LUT_TAG}"
    execute "./genLUT.sh dumpAll card=${CARD_CONFIG}"
    echo -e "\n\n"
}

function write_l1_db_file() {
    local L1TRGOBJS_TXT=$1
    local DB_FILENAME=$2

    echo "Writing out L1TriggerObjects for ${LUT_TAG} to DB file"
    execute "./writetoSQL.csh ${THE_YEAR} L1TriggerObjects ${L1TRGOBJS_TXT} Tag 1 ${DB_FILENAME}"
    execute "cp ${DB_FILENAME} ${EOS_DIR}"

    echo -e "\n\n"
}

function l1_db_routine() {
    local DB_FILE_RETURN=$1
    local TRIGGER_KEY_XML=$2

    local L1TRGOBJS_TXT="conditions/${LUT_TAG}/Deploy/Gen_L1TriggerObjects_${LUT_TAG}.txt"
    execute "cp ${L1TRGOBJS_TXT} ."

    execute "cp ${CMSSW_BASE}/src/ConditionsValidation/Tools/writetoSQL.csh ."
    chmod +x writetoSQL.csh
    echo -e "\n\n"
    
    local DB_FILE=HcalL1TriggerObjects_${LUT_TAG}.db

    write_l1_db_file $(basename ${L1TRGOBJS_TXT}) ${DB_FILE}

    eval "${DB_FILE_RETURN}=${EOS_DIR}/${DB_FILE}"
    
    # Update trigger objects tag in the trigger key XML (only relevant for new conditions)
    if [[ -n "${TRIGGER_KEY_XML}" && -f "${TRIGGER_KEY_XML}" ]]; then
        sed -i "s/HcalL1TriggerObjects_v2.0_hlt/${L1TRGOBJS_TAG}/g" ${TRIGGER_KEY_XML}
    fi
}

function update_conditions() {
    local REF_OR_NEW=$1
    local CONDITION=$2

    if [[ ${REF_OR_NEW} == "new" ]]; then
        local NICKNAME=$3

        echo "Modifying ${CONDITION} text files and copying into conditions working area"

        execute "construct_${NICKNAME}_txt conditions/${CONDITION}/${CONDITION}_Run${FUTURE_RUN}.txt ${CONDITION}.txt temp${CONDITION}.txt"

        # Reformat final constructed effective pedestals txt
        reformat_conditions_txt ${CONDITION} temp${CONDITION}.txt temp${CONDITION}Formatted.txt

        # And then copy the constructed pedestal txt into the conditions area
        mv temp${CONDITION}Formatted.txt conditions/${CONDITION}/${CONDITION}_Run${FUTURE_RUN}.txt
    else
        local PREV_CONDS_DIR=$3
        execute "cp ${PREV_CONDS_DIR}/${CONDITION}.txt conditions/${CONDITION}/${CONDITION}_Run${FUTURE_RUN}.txt"
    fi
}

###############################################################################################################
# End function definitions, now executing

execute "unpack_conditions ${PEDESTALS_TAR}"
execute "unpack_conditions ${GAINS_TAR}"

CONFIG_CARD="cardPhysics.sh"

# Condition for making "new" conditions with updated pedestals and/or gains
if [[ ${NEW_OR_REF} == "new" ]]; then

    dump_conditions ${CONFIG_CARD}

    # First inject new conditions txt files into the dumped conditions area
    if [[ -f ${PEDESTALS_TAR} ]]; then
        update_conditions ${NEW_OR_REF} EffectivePedestals pedestals
        update_conditions ${NEW_OR_REF} EffectivePedestalWidths pedestals
    fi

    if [[ -f ${GAINS_TAR} ]]; then
        update_conditions ${NEW_OR_REF} Gains gains
    fi

    # Now can generate LUT, TK, L1TriggerObjects txt
    echo "Generating LUT and TK"
    execute "./genLUT.sh generate card=${CONFIG_CARD}"
    echo -e "\n\n"

    LUT_XML="conditions/${LUT_TAG}/${LUT_TAG}.xml"
    TK_XML="conditions/${LUT_TAG}/tk_${LUT_TAG}.xml"

    # Need to replace tag names for effective pedestal, pedestal widths, etc. manually in TK
    # as they are not changed based on GT, but based on in-situ update of text files above
    if [[ -f ${PEDESTALS_TAR} ]]; then
        echo "Changing tag name for effective pedestals and pedestal widths in TK"
        sed -i "s/HcalPedestals_ADC_v8.1_hlt_effective/${EFFPEDS_TAG}/g" ${TK_XML}
        sed -i "s/HcalPedestalWidths_ADC_v8.1_hlt_effective/${EFFPEDWIDTHS_TAG}/g" ${TK_XML}
    fi

    if [[ -f ${GAINS_TAR} ]]; then
        echo "Changing tag name for gains in TK"
        sed -i "s/HcalGains_v3.0_hlt/${GAINS_TAG}/g" ${TK_XML}
    fi

    echo "Packing up conditions area including generated LUT and TK"
    CONDITIONS_ARCHIVE=""
    tar_and_cp CONDITIONS_ARCHIVE
    echo -e "\n\n"

    # Keep LUT and TK in tmp folder until ready so XMLDB uploader doesn't grab it too early
    STASH_DIR=${EOS_DIR}/tmp
    mkdir -p ${STASH_DIR}

    LUT_XML_OUTPATH=${STASH_DIR}/$(basename ${LUT_XML})
    TK_XML_OUTPATH=${STASH_DIR}/$(basename ${TK_XML})
    CONFIG_OUTPATH=${EOS_DIR}/cardPhysics_${NEW_OR_REF}.sh

    execute "cp ${TK_XML} ${STASH_DIR}"
    execute "cp ${LUT_XML} ${STASH_DIR}"
    execute "cp ${CONFIG_CARD} ${CONFIG_OUTPATH}"

    DB_FILE_OUTPATH=''
    l1_db_routine DB_FILE_OUTPATH ${TK_XML_OUTPATH} 

# Condition to make "ref" LUT/L1 DB file based last measured pedestals and/or gains
else
    dump_conditions ${CONFIG_CARD}

    # For "ref" conditions, pull pedestals and/or gains from latest cache area and just copy them in
    if [[ -f ${PEDESTALS_TAR} ]]; then
        update_conditions ${NEW_OR_REF} EffectivePedestals ${PREV_PEDESTALS_DIR}
        update_conditions ${NEW_OR_REF} EffectivePedestalWidths ${PREV_PEDESTALS_DIR}
    fi

    if [[ -f ${GAINS_TAR} ]]; then
        update_conditions ${NEW_OR_REF} Gains ${PREV_GAINS_DIR}
    fi

    # After copying in, can generate L1TriggerObjects txt
    echo "Generating LUT and TK for ref conditions"
    execute "./genLUT.sh generate card=${CONFIG_CARD}"

    echo "Packing up conditions area including generated LUT and TK"
    CONDITIONS_ARCHIVE=""
    tar_and_cp CONDITIONS_ARCHIVE
    echo -e "\n\n"

    LUT_XML_OUTPATH="None"
    TK_XML_OUTPATH="None"
    CONFIG_OUTPATH=${EOS_DIR}/cardPhysics_${NEW_OR_REF}.sh

    execute "cp ${CONFIG_CARD} ${CONFIG_OUTPATH}"

    DB_FILE_OUTPATH=''
    l1_db_routine DB_FILE_OUTPATH
fi

ecalautomation.py ${TASK} jobctrl --done --id ${JOB_ID} --fields "lut:${LUT_XML_OUTPATH}" "config_card:${CONFIG_OUTPATH}" "conditions_tar:${CONDITIONS_ARCHIVE}" "trigger_key:${TK_XML_OUTPATH}" "pedestals_input:${PEDESTALS_TAR}" "gains_input:${GAINS_TAR}" "l1_trg_objs_db:${DB_FILE_OUTPATH}" "pedestals_tag:${EFFPEDS_TAG}" "pedestalwidths_tag:${EFFPEDWIDTHS_TAG}" "gains_tag:${GAINS_TAG}"
