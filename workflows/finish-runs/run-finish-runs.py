from ecalautoctrl import process_by_fill
from hcal_condor import HCALCondorHandler
from hcal_runtools import allowGroupOverride

import sys
import time


@allowGroupOverride
@process_by_fill(fill_complete=False)
class FinishRunsHandler(HCALCondorHandler):
    def __init__(self, task_name, script_path, **kwargs):
        super().__init__(task_name, script_path, **kwargs)

        self.augmentParser(self.submit_parser)
        self.submit_parser.add_argument("--run-range",     type = str, default = None)
        self.submit_parser.add_argument("--task-statuses", type = str, default = None)

        self.prevTaskNames = kwargs["deps_task"]


    def getWorkItems(self):
        self.reconfigMMlogger()

        runStr          = self.opts.run_range
        taskStatusesStr = self.opts.task_statuses

        taskStatuses = taskStatusesStr.split(",")

        firstRun = None
        lastRun  = None
        if "-" in runStr:
            runRange = runStr.split("-")
            firstRun = int(runRange[0])
            lastRun  = int(runRange[-1])
        else:
            firstRun = int(runStr)
            lastRun  = int(runStr)

        for task in self.allHcalTaskNames:
            for status in taskStatuses:
                runs = self.rctrl.getRuns(status = {task : status}) 
                for run in runs:
                    runNumberStr = run["run_number"]
                    runNumberInt = int(runNumberStr)

                    if runNumberInt >= firstRun and runNumberInt <= lastRun:
                        self.log.debug(f"Setting task \"{task}\" for run {runNumberStr} to \"{self.finishedTaskStatus}\".")
                        self.rctrl.updateStatus(run = runNumberStr, status = {task : self.finishedTaskStatus})
                        time.sleep(0.1)


if __name__ == "__main__":

    handler = FinishRunsHandler("finish-runs", "", deps_task = None)
    ret_code = handler()
    sys.exit(ret_code)
