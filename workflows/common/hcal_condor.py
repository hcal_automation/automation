from ecalautoctrl import HandlerBase, JobCtrl
from hcal_dbs import HCALQueryDBS

from typing import List, Dict, Any
from pathlib import Path

import argparse
import htcondor
import classad
import os
import shutil
import time
import logging


class HCALCondorHandler(HandlerBase):

    def __init__(self, task: str, script_path, deps_tasks: List[str] = None, **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)

        self.parsers = (self.submit_parser, self.resubmit_parser)
        for parser in self.parsers:
            parser.add_argument("--env-name",      type = str)
            parser.add_argument("--env-base-path", type = str)
            parser.add_argument("--req-disk",      type = str, default = "1GB")
            parser.add_argument("--req-mem",       type = str, default = "1GB")
            parser.add_argument("--req-cpus",      type = str, default = "1")
            parser.add_argument("--job-flavor",    type = str, default = "\"workday\"")

        self.scriptPath = script_path
        self.scriptName = Path(self.scriptPath).name

        self.env_name = None

        self.eosOutputDir    = None
        self.publicOutputDir = None
        self.publicSubDir    = None

        credd = htcondor.Credd()
        credd.add_user_cred(htcondor.CredTypes.Kerberos, None)
        self.schedd = htcondor.Schedd()

        self.useCosmics = False

        # Override some logging settings from TaskHandler class
        self.log.setLevel(level = logging.DEBUG)
        self.loggingPayloadFormat = f'[{str(self.appname)}] at %(asctime)s.%(msecs)03d [%(levelname)s]: %(message)s'
        self.loggingTimeFormat = "%Y-%m-%d %H:%M:%S"
        self.log.handlers[0].setFormatter(logging.Formatter(self.loggingPayloadFormat, self.loggingTimeFormat))
        self.log.handlers[0].setLevel(logging.DEBUG)

        # Protected status keywords that correspond to a task in a good state
        self.finishedTaskStatus       = "done"
        self.defaultTaskStatus        = "new"
        self.processingTaskStatus     = "processing"
        self.reprocessTaskStatus      = "reprocess"
        self.incompleteTaskStatus     = "incomplete"
        self.ongoingTaskStatuses      = [self.reprocessTaskStatus, self.processingTaskStatus]
        self.ineligibleFillTaskStatus = "ineligiblefill"
        self.ineligibleRunTaskStatus  = "nogoodlumis"

        self.allHcalTaskNames = ["pedestals-production", "pedestals-validation",
                                 "gains-production",     "gains-validation",
                                 "lut-production",       "lut-validation",
                                 "hlt-rates-production", "hlt-rates-validation",
                                 "l1t-ntuples-production", "l1t-rates-production", "l1t-rates-validation",
                                 "upload-conditions"]

        # Common string used everywhere
        self.runAndFill = None


    def getWorkItems(self, group) -> Dict[str, Any]:
        raise NotImplementedError


    def getFilesDBS(self, lumiList = None):
        fromt0 = self.opts.t0 if "t0" in self.opts else False
        dataQuery = HCALQueryDBS(logger = self.log)
        ret = dataQuery.getFilesForRun(datasetPattern = self.dataSetName, runNumber = self.runForFiles, lumiList = lumiList, fromt0 = fromt0)

        return ret


    # For specific cluster(s), get associated condor jobs and their ads
    def getJobs(self, clusters, jid=None):
        if not isinstance(clusters, list):
            clusters = [clusters]
        constraint = "( " + " || ".join([f"ClusterId == {c}" for c in clusters]) + " ) "
        if jid is not None:
            constraint += f"&& ProcId == {jid}"

        ads = None
        retries = 0
        while ads == None:
            if retries > 4:
                return None
            try:
                ads = self.schedd.query(
                    constraint = constraint,
                    projection = ["ClusterId", "ProcId", "JobStatus"]
                )
            except Exception as err:
                retries += 1
                self.log.warning(f"Could not query the condor scheduler due to \"{err}\". Trying again...")
                time.sleep(30)
           
        return list(ads)


    # Watch the condor jobs every x number of seconds
    def wait(self, clusters, timeInterval = 5):
        states = {"idle": 1, "running": 2, "held": 5}
        
        # Map runNumber to whether its condor job cluster contains any held jobs
        stuckJobs = {}

        # Use list of cluster runNumbers to keep track of which cluster is done
        waitingOn = list(clusters.keys())
        clusterPrevStatus = {}
        while True:
            for runNumber, cluster in clusters.items():
      
                # If jobs is None, then there was issue querying the condor scheduler
                # so skip the rest of the loop and go again
                jobs = self.getJobs(cluster)
                if jobs != None:

                    results = {}
                    for state, val in states.items():
                        results[state] = len([x for x in jobs if x["JobStatus"] == val])

                    jobStatusStr = " | ".join(["%s: %s"%(str(state), str(njobs)) for state, njobs in results.items()])
                    logMessage = f"Periodic check of condor jobs for task \"{self.task}\" for {self.runAndFill}: {jobStatusStr}"
                    if cluster in clusterPrevStatus:
                        if jobStatusStr != clusterPrevStatus[cluster]:
                            self.log.debug(logMessage)
                            clusterPrevStatus[cluster] = jobStatusStr
                    else:
                        self.log.debug(logMessage)
                        clusterPrevStatus[cluster] = jobStatusStr

                    # If jobs becomes held for a cluster, track it via the runNumber
                    if results["idle"] == 0 and results["running"] == 0:
                        if results["held"] == 0:
                            stuckJobs[runNumber] = False
                        else:
                            stuckJobs[runNumber] = True

                        # No further jobs to process for runNumber cluster
                        waitingOn.remove(runNumber)
            
            if waitingOn:
                time.sleep(timeInterval)
            else:
                return stuckJobs



    # Helper function for crafting and submitting condor job based on passed arguments
    def doCondorSubmit(self, workItems, **kwargs):

        # Prepare to pass along public-facing output directory only if the subdir is not None
        publicDirStr = f" {self.publicOutputDir} " if self.publicSubDir else " "

        for job_id, item in enumerate(workItems):
            item["arguments"] = f"\"{self.env_name} {self.scriptName} '{kwargs['task']}' {job_id} {kwargs['eosOutputDir']}{publicDirStr}{item.get('arguments', '')}\""

            item["transfer_input_files"] = f"$(x509userproxy), {self.scriptPath}"

        sub_result = None
        try:
            sub_result = self.schedd.submit(htcondor.Submit(kwargs["condorJobAds"]), itemdata = iter(workItems))
        except Exception as err:
            self.log.error(f"While submitting jobs to condor, an exception occurred with reason: {err}.")
            return None, None

        cluster_id = sub_result.cluster()
        pids = list(range(sub_result.first_proc(), sub_result.first_proc() + sub_result.num_procs()))

        if len(pids) != len(workItems):
            self.log.error(f"Not all work items for task \"{self.task}\" successfully submitted as condor jobs for {self.runAndFill} - expected {len(workItems)} jobs but found {len(pids)}...")
            return None, None
        else:
            self.log.info(f"Submitted {len(workItems)} job(s) to condor for task \"{self.task}\" for {self.runAndFill}.")

        parameters = [
            {
                "group":      f"{kwargs['runNumber']}",
                "cluster-id": cluster_id,
                "proc-id":    pids[i],
                **{f"condor_val__{attribute}": value for attribute, value in work_item.items()}
            }
            for i, work_item in enumerate(workItems)
        ]

        return parameters, cluster_id


    # Checks the previous task status and determines whether the current task should continue
    # while returning jobs from the previous task in order to get the output
    def commonTaskStatusGetter(self, runObj):

        # The only tasks that do not have a previous task are ntuple-production
        # and in that case, the previous task of None is assumed to always be "done"
        if not self.prevTaskNames:
            return self.finishedTaskStatus, None

        runNumber  = runObj["run_number"]
        fillNumber = runObj["fill"]

        # There may be more than one previous task that the current task depends on.
        # Loop over each previous task and return for worst case scenario
        # i.e. if just one previous task is not done, then return with its unique status
        for prevTaskName in self.prevTaskNames:
            prevTask = JobCtrl(
                task     = prevTaskName,
                campaign = self.opts.campaign[0],
                tags     = {"run_number": runNumber, "fill": fillNumber},
                dbname   = self.opts.dbname,
            )

            prevStatuses   = self.rctrl.getStatus(runNumber)
            prevTaskStatus = prevStatuses[0][prevTaskName]

            boilerPlateMessage = f"Previous task \"{prevTaskName}\" with status \"{prevTaskStatus}\" for {self.runAndFill}"

            # Easiest case, if previous task is done then nothing left to check or worry about
            if prevTaskStatus == self.finishedTaskStatus:
                continue
    
            # The previous task is marked for reprocess and needs to be rerun before current task
            elif prevTaskStatus == self.reprocessTaskStatus:
                self.log.debug(f"{boilerPlateMessage} needs to be rerun first.")
                return prevTaskStatus, self.reprocessTaskStatus

            # For a previous task that is ongoing, also return the proposed new status for current task as "reprocess"
            elif prevTaskStatus == self.processingTaskStatus:
                self.log.debug(f"{boilerPlateMessage} has ongoing jobs and needs to complete first.")
                return prevTaskStatus, self.reprocessTaskStatus

            # If somehow the previous task has status "new" then current task must wait until previous is run
            elif prevTaskStatus == self.defaultTaskStatus:
                self.log.debug(f"{boilerPlateMessage} has not been run yet and needs to complete first.")
                return prevTaskStatus, self.reprocessTaskStatus

            # For task status of anything else, it assumed that previous task should prevent current task
            # E.g. if pedestal production task is ineligiblefill
            else:
                self.log.debug(f"{boilerPlateMessage} prevents current task \"{self.task}\" from proceeding.")
                return prevTaskStatus, prevTaskStatus
                   

        # If we make it here, then all previous tasks
        # should be completed and can return as such
        return prevTaskStatus, None

    # The same core functionality for checking status of task
    # is called by the getWorkItems function
    def getPreviousTaskOutput(self, runObj, fieldName = "output"):

        prevTaskStatus, status = self.commonTaskStatusGetter(runObj)

        runNumber  = runObj["run_number"]
        fillNumber = runObj["fill"]

        # We know already that the pedestal tasks all only have one previous task
        boilerPlateMessage = f"Previous task \"{self.prevTaskNames[0]}\" with status \"{prevTaskStatus}\" for {runNumber} (fill {fillNumber})"

        # Assumption is a pedestal task will have one job...
        prevTask = JobCtrl(
            task     = self.prevTaskNames[0],
            campaign = self.opts.campaign[0],
            tags     = {"run_number": runNumber, "fill": fillNumber},
            dbname   = self.opts.dbname,
        )
        prevTaskJobs = prevTask.getJob(0, last = True)
        if prevTaskJobs:
            # If we make it to here, then the previous task is assumed done
            prevTaskJob = prevTaskJobs[0]
            outputFile  = prevTaskJob.get(fieldName, None)
            if not outputFile:
                self.log.error(f"{boilerPlateMessage} did not return an output file, please review the corresponding Jenkins workflow, as the current task \"{self.task}\" cannot continue.")
                return None, self.incompleteTaskStatus

            auxArgs = getattr(self.opts, f"prev_{fieldName}_dir", " ")

            args = [{"arguments": f"{outputFile} {runNumber} {auxArgs}"}]

            return args, None

        # Propagate along the unique status when previous jobs not available/ready
        else:
            return None, status


    def checkAndUpdateTask(self, task, run, fill, jctrl):

        # Here, these statuses refer to the "jobs" for the task as stored in the ECAL automation DB
        # These statuses are not literal condor job statuses
        failed  = []
        idle    = []
        done    = []
        running = []

        status = self.rctrl.getStatus(run)[0][task]

        # See if any running and idle jobs and try and update their status
        for jid in jctrl.getRunning() + jctrl.getIdle():
            aJob = jctrl.getJob(jid=jid, last=True)[-1]

            cid = aJob["cluster-id"]
            pid = aJob["proc-id"]

            # If condorJobs is None, then there was an issue querying the scheduler
            # so we cannot do anything about updating the status of the job
            # Note that we are getting a single condor job here that will be wrapped as a list...
            condorJob = self.getJobs(cid, pid)
            if condorJob != None:
                # First check if we have empty list i.e. no know condor jobs
                # Assume worst case and fail the job in the database
                if condorJob == []:
                    self.log.debug(f"Job {jid} for task \"{task}\" for {self.runAndFill} is designated as ongoing in the database, but has no known condor job, so marking as failed.")
                    jctrl.failed(jid=jid)

                # Check if a job is in a held state in the condor cluster, meaning it has failed
                # These jobs are removed from condor and the task needs to be run again
                elif condorJob[0]["JobStatus"] == 5:
                    self.log.warning(f"Job {cid}.{jid} for task \"{task}\" for {self.runAndFill} is held and will be failed and removed from condor.")
                    jctrl.failed(jid=jid)
                    self.schedd.act(htcondor.JobAction.Remove, f"JobId == {condorJob[0]['ClusterId']}.{condorJob[0]['ProcId']}")

        # Following job updating, get the latest list of job types
        failed  = jctrl.getFailed()
        done    = jctrl.getDone()
        idle    = jctrl.getIdle()
        running = jctrl.getRunning()
    
        # Put the task (if not already) into the processing status if still running or idle jobs
        # Don't worry about failed or held, will worry once all jobs are processed i.e. not running or idle
        if running or idle:
            if status != self.processingTaskStatus:
                self.rctrl.updateStatus(run = run, status = {task: self.processingTaskStatus})

        # Nothing is running or idle, but jobs are failed or held, set the status to reprocess
        elif failed:
            self.rctrl.updateStatus(run = run, status = {task: self.reprocessTaskStatus})
            self.log.error(f"Task \"{task}\" for {self.runAndFill} did not complete successfully.")

        # No running or idles jobs, and not held or failed jobs, so if task has completed jobs
        # we can transition it to done
        else:
            if done and status == self.processingTaskStatus:
                self.log.info(f"Task \"{task}\" for {self.runAndFill} is done.")
                self.rctrl.updateStatus(run = run, status = {task: self.finishedTaskStatus})



    # Construct dictionary of condor jobs parameters a la what would go in a jdl file
    def getSkeleton(self, runNumber):

        wrapper = Path(__file__).parent / "condor_wrapper.sh"
        shutil.copy(wrapper, ".")

        condorLogDir = f"{self.opts.env_base_path}/condor_output/xfer"
        homeDir      = f"{self.opts.user_grid_proxy.split('/private')[0]}"

        condorJobAds = {
            "executable"              : "condor_wrapper.sh",
            "output"                  : f"{self.task}_{runNumber}.$(ClusterId)_$(ProcId).out",
            "error"                   : f"{self.task}_{runNumber}.$(ClusterId)_$(ProcId).err",
            "log"                     : f"{homeDir}/automation_logs/{self.task}_{runNumber}.$(ClusterId)_$(ProcId).log",
	        "requirements"            : '(OpSysAndVer =?= "AlmaLinux9")',
            "request_cpus"            : self.opts.req_cpus,
            "request_memory"          : self.opts.req_mem,
            "request_disk"            : self.opts.req_disk,
            "should_transfer_files"   : "YES",
            "transfer_output_files"   : "",
            "when_to_transfer_output" : "on_success",            
            "output_destination"      : f"root://eosuser.cern.ch//{condorLogDir}/",
            "MY.XRDCP_CREATE_DIR"     : "True",
            "getenv"                  : "HCAL* ECAL*",
            "+JobFlavour"             : f'"{self.opts.job_flavor}"',
            "x509userproxy"           : self.opts.user_grid_proxy,
            "MY.SendCredential"       : "true",
        }

        return condorJobAds


    def submit(self):
        self.reconfigMMlogger()

        self.env_name     = self.opts.env_name
        self.useCosmics   = self.opts.use_cosmics
        self.publicSubDir = self.opts.public_subdir

        # The upload-conditions pipeline needs an explicit run number from the GUI
        # Running over all runs with "new" status (run number field in GUI left empty) is to be avoided
        if self.task == "upload-conditions" and not self.opts.the_run:
            self.log.error("A user attempted to run the hcal-UploadConditions pipeline without specifying a run number via the GUI. The pipeline will not be processed any further !")
            return

        # The only task that needs to care whether running on
        # cosmics or collisions is the pedestal genesis task "ntuple-production".
        # All downstream tasks do not have to care and can just blissfully do their thing
        # for whichever runs they find
        caresIfCosmics = self.task == "ntuple-production"

        allClusters = {}

        jctrlStructs = []

        # For a case of running finish-runs or finish-jobs,
        # run the getWorkItems, do the work, and be done
        if self.task == "finish-runs":
            self.getWorkItems()
            return

        elif self.task == "finish-jobs":
            workItems = self.getWorkItems()

            condorJobAds = self.getSkeleton(self.opts.the_run)

            self.runAndFill = f"run {self.opts.the_run} (fill {self.opts.the_fill})" 

            parameters, cluster = self.doCondorSubmit(workItems, runNumber = self.opts.the_run, condorJobAds = condorJobAds, task = None, eosOutputDir = None)

            if parameters and cluster:            
                allClusters[str(self.opts.the_run)] = cluster
                self.wait(allClusters)
            
            return

        # Main loop for "job" submission is the loop of run groups
        # Based on the inherited method process_by_fill, the groups
        # are sorted by run, earliest to latest, and the runs within a 
        # group are also sorted earliest to latest
        # Run groups only composed of runs with task in status "new" or "reprocess"
        for group in self.groups():

            # Loop over all runs for a group i.e. fill
            # Determining if good run done by getWorkItems()
            for runObj in group:

                runNumber       = runObj["run_number"]
                fillNumber      = runObj["fill"]
                self.runAndFill = f"run {runNumber} (fill {fillNumber})" 

                self.eosOutputDir    = f"{self.opts.eosdir}/{self.task}/{runNumber}"
                self.publicOutputDir = self.eosOutputDir.replace(self.task, self.publicSubDir) if self.publicSubDir else None

                # This method makes updates to the status of the previous task based on job status
                # and then returns the updated previous-task status
                # If previous task status is not "done", then do not need to proceed further
                # The proposedNewTaskStatus is determined based on the previous task status
                prevTaskStatus, proposedNewTaskStatus = self.commonTaskStatusGetter(runObj)
                if prevTaskStatus != self.finishedTaskStatus and proposedNewTaskStatus:
                    self.rctrl.updateStatus(run = runNumber, status = {self.task: proposedNewTaskStatus})

                    continue

                if self.task == "hlt-rate-trends":
                   self.getWorkItems(runObj)

                   return
                  
                # Only care about fill being dumped for collisions, not cosmics
                if caresIfCosmics and not self.useCosmics and not self.rctrl.fillDumped(fill = fillNumber):
                    self.log.debug(f"Fill {fillNumber} is still ongoing and not dumped, so current task \"{self.task}\" will be tried again later.")

                    break

                currentTaskStatus = self.rctrl.getStatus(runNumber)[0][self.task]

                # Generate a condor jdl prescription for the run
                # and initialize a job control task
                condorJobAds = self.getSkeleton(runNumber)
                jctrl = JobCtrl(
                    task     = self.task,
                    campaign = self.campaign,
                    tags     = {"run_number": str(runNumber), "fill": fillNumber},
                    dbname   = self.opts.dbname,
                )

                self.log.debug(f"Task {self.task} for {self.runAndFill} is in the state \"{currentTaskStatus}\"")

                # For any remaining actions to be taken, either the job task should be new and not in the DB
                # or it was determined that the job task needs to be reprocessed
                if not jctrl.taskExist() or currentTaskStatus == self.reprocessTaskStatus:
                    task = f'-w {self.task} -c {self.campaign} -t fill:{fillNumber},run_number:{runNumber} --db {self.opts.dbname}'

                    # See individual workflow child classes for specific implementation of getWorkItems
                    # If there are no work items for a workflow task, update the status, check for unique
                    # status updates
                    workItems, taskStatusUpdate = self.getWorkItems(runObj)

                    # Any task status that applies to a fill as a whole can be used
                    # to update the corresponding task status for all runs for a fill
                    # with no further processing here
                    if taskStatusUpdate == self.ineligibleFillTaskStatus:
                        self.log.debug(f"Current run in group for fill {fillNumber} has task status \"{taskStatusUpdate}\" for \"{self.task}\", which disqualifies the fill as a whole. Setting task status for all tasks to \"{taskStatusUpdate}\" for all runs in current group.")
                        for tempRunObj in group:
                            for aTask in self.allHcalTaskNames:
                                self.rctrl.updateStatus(run = tempRunObj["run_number"], status = {aTask: taskStatusUpdate})
                        break

                    # Not as harsh as an ineligible fill, but for an ineligible run
                    # set all task status for that run to the status update and continue
                    if taskStatusUpdate == self.ineligibleRunTaskStatus:
                        self.log.debug(f"Current run in group for fill {fillNumber} has task status \"{taskStatusUpdate}\" for \"{self.task}\", which disqualifies the run overall. Setting task status for all tasks to \"{taskStatusUpdate}\" for this run.")
                        for aTask in self.allHcalTaskNames:
                            self.rctrl.updateStatus(run = runNumber, status = {aTask: taskStatusUpdate})

                        continue

                    # Check if there is a new status when there are work items
                    if taskStatusUpdate and currentTaskStatus != taskStatusUpdate:
                        self.log.debug(f"Changing status for task \"{self.task}\" for {self.runAndFill} to \"{taskStatusUpdate}\".")
                        self.rctrl.updateStatus(run = runNumber, status = {self.task: taskStatusUpdate})

                    # No work items means nothing to submit
                    if not workItems:
                        continue

                    parameters, cluster = self.doCondorSubmit(workItems, runNumber = runNumber, condorJobAds = condorJobAds, task = task, eosOutputDir = self.eosOutputDir)
                    if not parameters and not cluster:
                        continue

                    # If the directory already exists, clear it out
                    # Check for double slashes which could suggest empty variables
                    # used for constructing eosOutputDir to avoid bad times during removal
                    if os.path.exists(self.eosOutputDir) and "//" not in self.eosOutputDir:
                        self.log.debug(f"Removing already-existing output folder \"{self.eosOutputDir}\" for task \"{self.task}\" for {self.runAndFill} and recreating.")
                        shutil.rmtree(self.eosOutputDir)

                    # Make the EOS output directory for the task/run
                    os.makedirs(self.eosOutputDir, exist_ok = True)

                    # Create the public-facing directory containing validation type plots
                    if self.publicOutputDir:
                        # Furthest down subdir should be the run number
                        smallestSubdir = self.publicOutputDir.split("/")[-1]
                        if os.path.exists(self.publicOutputDir) and smallestSubdir.isdigit():
                            self.log.debug(f"Removing already-existing public output folder \"{self.publicOutputDir}\" for task \"{self.task}\" for {self.runAndFill} and recreating.")
                            shutil.rmtree(self.publicOutputDir)

                        # Make the public output directory for the task/run validation plots
                        os.makedirs(self.publicOutputDir)

                    allClusters[runNumber] = cluster

                    jctrl.createTask(
                        jids     = list(range(len(workItems))),
                        recreate = True,
                        fields   = parameters,
                    )

                    time.sleep(1)
                    self.rctrl.updateStatus(run = runNumber, status = {self.task: self.processingTaskStatus})

                    jctrlStructs.append({"run" : runNumber, "fill" : fillNumber, "jctrlObj" : jctrl})

        # Here if we've appended cluster IDs to the list, then we have tasks with submitted jobs
        # and in that case can wait and check the individual clusters
        # The return of the wait is a dict mapping each cluster (runNumber) and whether it has stuck jobs
        if allClusters:
            runNumHasStuckJobs = self.wait(allClusters)

            # If there are held jobs (regardless of failed) the task should be marked as "incomplete"
            # which implies manual intervention to clean up jobs and rerun the task
            for run, hasStuckJobs in runNumHasStuckJobs.items():
                if hasStuckJobs:
                    self.log.warning(f"Latest batch of condor jobs for task \"{self.task}\" for {self.runAndFill} have been processed, however, there are held jobs. The task will be marked \"{self.incompleteTaskStatus}\" and held jobs need to be cleaned up.")
                    self.rctrl.updateStatus(run = run, status = {self.task: self.incompleteTaskStatus})
                else:
                    self.log.info(f"Latest batch of condor jobs for task \"{self.task}\" for {self.runAndFill} have been processed.")

        # All unique jobs for the unique runs have been processed, now update their status in the InfluxDB
        for jctrlStruct in jctrlStructs:
            self.checkAndUpdateTask(self.task, jctrlStruct["run"], jctrlStruct["fill"], jctrlStruct["jctrlObj"])

        return 0


    def reconfigMMlogger(self, level = "CRITICAL"):
        self.mm_handler.setFormatter(logging.Formatter(self.loggingPayloadFormat, self.loggingTimeFormat))
        self.mm_handler.setLevel(getattr(logging, level))
