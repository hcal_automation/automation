from typing import List, Dict, Optional
from omsapi import OMSAPI, OMS_FILTER_OPERATORS

import time
import urllib3
import logging

urllib3.disable_warnings()

# add missing operator to OMS (check in the future)
OMS_FILTER_OPERATORS.append("CT")


class HCALQueryOMS:

    def __init__(self, logger):
        self.oms = OMSAPI("https://cmsoms.cern.ch/agg/api", "v1", cert_verify = False, verbose = False)
        self.oms.auth_oidc("ecalgit-omsapi", "KXbuy4vxiETBc5C7FwteQxAF3X1irilx")
        self.logger = logger

    @staticmethod
    def make_filter(items):
        return [dict(attribute_name = x[0], value = x[2], operator = x[1]) for x in items]


    def getFromOMS(
        self,
        item:    str  = "runs",
        filters: Dict = None,
        daq_completed = False,
        sort_by = None,
        attrs: Optional[List[str]] = ["run_number", "fill_number", "start_time", "end_time", "recorded_lumi"]
    ) -> Dict[int, Dict]:

        query = self.oms.query(item)
        if sort_by:
            query.sort(sort_by, asc = True)

        query.paginate(page = 1, per_page = 100000)
        query.attrs(attrs)

        if filters:
            query.filters(filters)

        queryAttempts = 0
        queryResponse = None
        while queryAttempts < 3:
            try:
                queryResponse = query.data()

                if "data" in queryResponse.json():
                    break

            except Exception as err:
                queryAttempts += 1
                self.logger.error(f"While trying to query the OMS database an exception occurred with reason: \"{err}\".")
                time.sleep(10)

        if queryResponse:
            queryResponseJSON = queryResponse.json()

            return [
                x["attributes"]
                for x in queryResponseJSON["data"]
                if not (daq_completed and not x["attributes"]["end_time"])
            ]
        else:
            self.logger.error(f"Unable to successfully query OMS (retries exceeded). Cannot return any results.")
            return []


    # For a specified collisions run (useCosmics = False), query for all lumi points occuring after start_time
    # For a specified cosmics run (useCosmics = True) filter on fill_type_runtime == "COSMICS"
    # and just get the subdetectors that were in the run and the last lumi section number
    def getLumiInfo(self, useCosmics = False, **kwargs):
        if not useCosmics:
            filters = None
            if "run_number" in kwargs:
                filters = [
                    dict(attribute_name = "run",      value = kwargs["run_number"], operator = "EQ"),
                    dict(attribute_name = "dip_time", value = kwargs["start_time"], operator = "GE"),
                    dict(attribute_name = "dip_time", value = kwargs["end_time"],   operator = "LE"),
                ]
            else:
                filters = [
                    dict(attribute_name = "dip_time", value = kwargs["start_time"], operator = "GE"),
                    dict(attribute_name = "dip_time", value = kwargs["end_time"],   operator = "LE"),
                ]
            return self.getFromOMS(
                item    = "diplogger/dip/CMS/BRIL/Luminosity",
                filters = filters,
                attrs   = ["inst_lumi", "dip_time", "lumi_section", "run"],
                sort_by = "dip_time",
            )
        else:
            filters = [
                dict(attribute_name = "run_number",        value = kwargs["run_number"], operator = "EQ"),
                dict(attribute_name = "fill_type_runtime", value = "COSMICS",            operator = "EQ"),
            ]
            return self.getFromOMS(
                item    = "runs",
                filters = filters,
                attrs   = ["components", "last_lumisection_number"],
            )


    # Specifying either a scope of "run" or "fill" and filtering on subsystem HCAL,
    # get all start and stop times of downtime from DAQ system
    def getDAQstatus(self, scope, number):
        filters = [
            dict(attribute_name = f"start_{scope}_number", value = number, operator = "EQ"),
            dict(attribute_name = "subsystem",             value = "HCAL", operator = "EQ"),
        ]
        return self.getFromOMS(
            item    = "downtimesrun3",
            filters = filters,
            attrs   = ["start_time", "stop_time"],
            sort_by = "start_time",
        )
