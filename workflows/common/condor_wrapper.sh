#!/usr/bin/env bash

AREA_NAME=$1
SCRIPT_NAME=$2
TASK=$3
JOBID=$4

shopt -s extglob

# Trap jobs that are terminated and "fail" the corresponding job in the database
function terminated() {
    ecalautomation.py ${TASK} jobctrl --id ${JOBID} --failed
    exit 255
}
trap terminated SIGTERM

# Trap job exit codes and "fail" the corresponding job in the database if code is not 0
function exitcheck() {
    exit_code=$1
    new_return_code=0
    if [[ ! ${exit_code} -eq 0 ]]; then
        ecalautomation.py ${TASK} jobctrl --id ${JOBID} --failed
        new_return_code=255
    fi

    # Cleanup before leaving to avoid copying things back to EOS
    # Don't remove out and err for jobs though
    rm -rf -- !(*_condor_*|hcalserv.cc)

    exit ${new_return_code}
}

# Define and export wrapper function to pass important function calls and exit 1 if something goes wrong
# This is useful to catch any issues and set the corresponding job to failed in the database
function execute() {
    echo "Executing: \"$1\""
    if ! eval $1; then
        exit 1
    fi
}
export -f execute

function print_args() {
    for VAR in "$@"; do
        echo "${VAR}: ${!VAR}"
    done
    echo -e "\n\n"
}
export -f print_args

# The txt from standalone pedestals and gains framework are formatted differently than native CMSSW format
# Do a "redump" routine within CMSSW to format the txt in the way they would come out of a cond dump
function reformat_conditions_txt() {
    # $1 = condition type e.g. Gains
    # $2 = input txt for reformatting
    # $3 = output txt from CMSSW redump

    cat << EOF >> temp_dump_cfg.py
import FWCore.ParameterSet.Config as cms
from Configuration.StandardSequences.Eras import eras

process = cms.Process("REDUMP",eras.Run3)

process.load("Configuration.StandardSequences.GeometryDB_cff")
process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_cff")

from Configuration.AlCa.autoCond import autoCond
process.GlobalTag.globaltag = autoCond["run3_data"] 

process.load('Configuration.StandardSequences.Services_cff')
process.load("CondCore.CondDB.CondDB_cfi")

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(1)
)
process.source = cms.Source("EmptySource",
    numberEventsInRun = cms.untracked.uint32(1),
    firstRun = cms.untracked.uint32(999999)
)

#--------------------- replacing conditions, fileinpath starts from /src 
process.es_ascii = cms.ESSource("HcalTextCalibrations",
   input = cms.VPSet(
       cms.PSet(
           object = cms.string("${1}"),
           file = cms.FileInPath("${2}")
      )
   )
)
process.es_prefer = cms.ESPrefer('HcalTextCalibrations','es_ascii')

process.dumpcond = cms.EDAnalyzer("HcalDumpConditions",
       dump = cms.untracked.vstring("${1}")
)
process.p = cms.Path(process.dumpcond)
EOF
    execute "cmsRun temp_dump_cfg.py"

    execute "mv Dump*.txt ${3}"
}
export -f reformat_conditions_txt

function construct_pedestals_txt() {
    local NOMINAL_CONDITIONS=$1
    local NEW_CONDITIONS=$2
    local COMBINED_CONDITIONS=$3

    # First the pedestals
    # Grab the header first
    sed -n "1,2p" ${NOMINAL_CONDITIONS} >> ${COMBINED_CONDITIONS}

    # Get the true HCAL channels from the new pedestals
    grep " HB " ${NEW_CONDITIONS} >> ${COMBINED_CONDITIONS} 
    grep " HE " ${NEW_CONDITIONS} >> ${COMBINED_CONDITIONS} 
    grep " HO " ${NEW_CONDITIONS} >> ${COMBINED_CONDITIONS} 
    grep " HF " ${NEW_CONDITIONS} >> ${COMBINED_CONDITIONS} 

    # Grab ZDC from the dumped conditions
    grep "ZDC" ${NOMINAL_CONDITIONS} >> ${COMBINED_CONDITIONS}
}
export -f construct_pedestals_txt

function construct_gains_txt() {
    local NOMINAL_CONDITIONS=$1
    local NEW_CONDITIONS=$2
    local COMBINED_CONDITIONS=$3

    # First, grab header and HB channels from the beginning of nominal Gains txt
    sed -n "1,1p" ${NOMINAL_CONDITIONS} >> ${COMBINED_CONDITIONS}
    grep " HB " ${NOMINAL_CONDITIONS} >> ${COMBINED_CONDITIONS}

    # Then, grab Gains produced from raddam reader (HE)
    grep " HE " ${NEW_CONDITIONS} >> ${COMBINED_CONDITIONS}
    grep " HF " ${NEW_CONDITIONS} >> ${COMBINED_CONDITIONS}

    # And then, send rest of channels from nominal Gains
    grep " HO " ${NOMINAL_CONDITIONS} >> ${COMBINED_CONDITIONS}
    grep "ZDC" ${NOMINAL_CONDITIONS} >> ${COMBINED_CONDITIONS}
}
export -f construct_gains_txt

###############################################################################################

export X509_USER_PROXY=$(realpath grid_proxy.x509)
xrdcp root://eosuser.cern.ch//eos/user/h/hcalserv/automation_working_area/${AREA_NAME}.tar.gz .
tar -xzf ${AREA_NAME}.tar.gz

ls -lrt
echo -e "\n\n\n"

source setup.sh
cd ${_CONDOR_SCRATCH_DIR}
echo -e "\n\n\n"

ls -lrt
echo -e "\n\n\n"

export NCPUS=$(condor_status -ads ${_CONDOR_MACHINE_AD} -af Cpus)

bash ${SCRIPT_NAME} "${@:3}"

scriptReturn=$?

exitcheck ${scriptReturn}
