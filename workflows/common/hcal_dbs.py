import urllib3
import logging
from dbs.apis.dbsClient import DbsApi
from typing import List, Dict, Optional


class HCALQueryDBS:

    def __init__(self, logger = None):
        self.dbs     = DbsApi("https://cmsweb.cern.ch/dbs/prod/global/DBSReader")
        self.logger  = logger


    def getFilesForRun(self, datasetPattern: Optional[str], runNumber: int, lumiList: Optional[List[int]] = None, fromt0 = False) -> list:

        datasetObjList = []
        try:
            datasetObjList = self.dbs.listDatasets(dataset = datasetPattern, run_num = int(runNumber))
        except Exception as err:
            self.logger.error(f"While querying DBS with data set pattern {datasetPattern}, an exception occurred with reason: {err}. Cannot return any data set names.")
            return []

        # Wildcards have been allowed for querying for the dataset name, but only at the convenience of not
        # writing the whole data set name out. If the pattern is too generic, multiple data set matches will
        # be returned, and it will no longer be clear how to proceed
        if len(datasetObjList) > 1:
            self.logger.error(f"DBS query for data set pattern \"{datasetPattern}\" returned more than one match. Cannot unambiguously proceed with getting list of files.")
            return []

        kwargs = dict(run_num = int(runNumber))
        if lumiList:
            kwargs["lumi_list"] = lumiList

        filePathPrefix = "file:/eos/cms/tier0" if fromt0 else "root://cms-xrd-global.cern.ch/"

        # Here by definition, the list contains just one data set name
        filePathList = []
        for datasetObj in datasetObjList:
            self.logger.debug(f"Getting files for data set \"{datasetObj['dataset']}\".")
    
            fileObjList = []
            try:
                fileObjList = self.dbs.listFiles(dataset = datasetObj["dataset"], **kwargs)
            except Exception as err:
                self.logger.error(f"While querying DBS for file list for data set \"{datasetObj['dataset']}\", an exception occurred with reason: {err}. No files can be listed.")
                return []

            for fileObj in fileObjList:
                self.logger.debug(f"Found file {fileObj['logical_file_name']} in DBS.")
                filePathList.append(filePathPrefix + fileObj["logical_file_name"])

        return filePathList
