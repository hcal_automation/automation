from ecalautoctrl.influxdb_utils import reformat_last

import time


def getRunObj(rctrl, runNumber, logger):
    dbQuery = f'SELECT *,"run_number" FROM (SELECT last(*) FROM "run" WHERE "campaign"=\'{rctrl.campaign}\' GROUP BY run_number,fill) GROUP BY "run_number"'

    data = []
    try:
        data = rctrl.db.query(dbQuery)
    except Exception as err:
        logger.error(f"While querying the InfluxDB for run {runNumber} an exception occurred with reason: \"{err}\"")
    
    data = reformat_last(data)
    data = [
        x
        for x in data
        if int(x["run_number"]) == runNumber and x["lumi"] != None
    ]

    return data


def maybeInt(val):
    if not val:
        return None
    return int(val)

def maybeBool(val):
    if not val:
        return None
    return val == "true"


def allowGroupOverride(cls):
    cls.base_group_func = getattr(cls, "groups", None)

    def augmentParser(self, parser):
        parser.add_argument("--val-run",            type = maybeInt,  default = None)
        parser.add_argument("--the-run",            type = maybeInt,  default = None)
        parser.add_argument("--dataset",            type = str,       default = None)
        parser.add_argument("--max-input-files",    type = maybeInt,  default = None)
        parser.add_argument("--use-cosmics",        type = maybeBool, default = None)
        parser.add_argument("--include-pedestals",  type = maybeBool, default = None)
        parser.add_argument("--include-gains",      type = maybeBool, default = None)
        parser.add_argument("--global-tag",         type = str,       default = None)
        parser.add_argument("--user-grid-proxy",    type = str,       default = None)
        parser.add_argument("--prev-pedestals-dir", type = str,       default = None)
        parser.add_argument("--prev-gains-dir",     type = str,       default = None)
        parser.add_argument("--public-subdir",      type = str,       default = None)

    def internal(self):
        if self.opts.the_run:
            runNumber = self.opts.the_run
            self.rctrl.updateStatus(run = runNumber, status = {self.task: self.reprocessTaskStatus})

            time.sleep(1)

            self.log.info(f"Detected user override from GUI to execute task \"{self.task}\" for run number {runNumber}.")

            # A single run object is returned by getRunObj here, need to wrap as a list of run objects
            return [getRunObj(self.rctrl, runNumber, self.log)]

        else:
            runObjList = []
            try:
                runObjList = self.base_group_func()
            except Exception as err:
                self.log.error(f"While querying the InfluxDB to get list of runs an exception occurred with reason: \"{err}\". Task \"{self.task}\" will process any further.")

            return runObjList


    setattr(cls, "groups",        internal)
    setattr(cls, "augmentParser", augmentParser)

    return cls
