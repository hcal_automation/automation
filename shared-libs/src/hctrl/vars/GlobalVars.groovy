#!/usr/bin/env groovy
package hctrl.vars

class GlobalVars{
    static def setenv(script, jobname)
    {
        script.env.hcal_working_area = "/eos/user/h/hcalserv/automation_working_area"

        script.env.dbinstance = "hcal_test_v2"
        script.env.eospath = "/eos/cms/store/group/dpg_hcal/comm_hcal/AUTOMATION_TEST_AREA"
        script.env.prev_pedestals_subdir = "validation/latest_pedestals"
        script.env.prev_gains_subdir = "validation/latest_gains"
        script.env.notify_url = "https://mattermost.web.cern.ch/hooks/q9dchgczn3ftdbrj1nqkabo6ma"            
        script.env.user_grid_proxy = "/afs/cern.ch/user/h/hcalserv/private/grid_proxy.x509"

        script.env.pedestals_val_public_subdir = "www/pedestals_validation"
        script.env.gains_val_public_subdir = "www/gains_validation"
        script.env.l1t_val_public_subdir = "www/l1t_rates_validation"
        script.env.hlt_val_public_subdir = "www/hlt_rates_validation"
        script.env.lut_val_public_subdir = "www/lut_validation"
        script.env.conds_upload_public_subdir = "www/conditions_upload_receipts"
    }
}
