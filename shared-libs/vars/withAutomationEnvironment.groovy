#!/usr/bin/env groovy

def call(String script, String working_area=env.hcal_working_area) {
    withCredentials([usernamePassword(credentialsId: 'hcalserv', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        s_script = """
ls -l $working_area
cd `find "$working_area" -maxdepth 1 -type d -name "CMSSW_*_*_*" | head -n 1`
set +x
cmsenv
set -x
cd -
$script"""
        sh(script: s_script)
    }
}
